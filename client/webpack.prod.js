const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const { InjectManifest } = require('workbox-webpack-plugin')
const { resolve } = require('path')

module.exports = merge(common, {
  mode: 'production',
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      inject: 'body'
    }),
    new InjectManifest({
      swSrc: './sw/service-worker.ts',
      swDest: 'sw.js'
    })
  ],
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 1024 * 1.5,
      cacheGroups: {
        vendor: {
          test: /([\\/]node_modules[\\/])(?!.*\b(three)\b)(.+)/,
          name (module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]

            // npm package names are URL-safe, but some servers don't like @ symbols
            return `npm.${packageName.replace('@', '')}`
          }
        }
      }
    },
    // innerGraph: true,
    // mangleWasmImports: true,
    minimize: true
    // minimizer: [new TerserWebpackPlugin()]
  },
  output: {
    filename: '[name].[contenthash].js',
    path: resolve(__dirname, '..', 'dist'),
    clean: true
  }
})
