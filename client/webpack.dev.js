const { merge } = require('webpack-merge')
const common = require('./webpack.common.js')
const { resolve } = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'eval-source-map',
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      inject: 'body',
      title: 'Development'
    })
  ],
  optimization: {
    runtimeChunk: true
  },
  devServer: {
    historyApiFallback: true,
    static: resolve(__dirname, '..', 'dist'),
    hot: true,
    devMiddleware: {
      writeToDisk: true
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
    }
  },
  output: {
    filename: '[name].bundle.js',
    path: resolve(__dirname, '..', 'dist'),
    clean: true
  }
})
