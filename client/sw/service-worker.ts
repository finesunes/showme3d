import { registerRoute } from 'workbox-routing'
import { StaleWhileRevalidate } from 'workbox-strategies'
import { ExpirationPlugin } from 'workbox-expiration'
import * as googleAnalytics from 'workbox-google-analytics'

googleAnalytics.initialize()

// @ts-ignore
// precacheAndRoute(self.__WB_MANIFEST)
// eslint-disable-next-line no-unused-expressions
self.__WB_MANIFEST

registerRoute(
  ({ request }) => request.destination === 'script' || request.destination === 'document' ||
    request.destination === 'style' || request.destination === 'manifest',
  new StaleWhileRevalidate({
    cacheName: 'application',
    plugins: [
      new ExpirationPlugin({
        maxAgeSeconds: 24 * 60 * 60
      })
    ]
  })
)

export {}
