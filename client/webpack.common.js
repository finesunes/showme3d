const { resolve } = require('path')

const CopyWebpackPlugin = require('copy-webpack-plugin')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin')
const Dotenv = require('dotenv-webpack')

module.exports = {
  entry: {
    index: './src/index.tsx'
    // 'file-loader-web-worker': './ww/fileLoader/index.ts'
    // 'service-worker': './sw/service-worker.ts'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'babel-loader',
        exclude: [/node_modules/, /sw/, /ww/]
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource'
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource'
      }
    ]
  },
  plugins: [
    new Dotenv(),
    new MomentLocalesPlugin({
      localesToKeep: ['es-us', 'ru']
    }),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: resolve(__dirname, 'src/manifest.json'),
          to: resolve(__dirname, '..', 'dist')
        },
        {
          from: resolve(__dirname, '..', 'node_modules/libarchive.js/dist'),
          to: resolve(__dirname, '..', 'dist', 'libarchive')
        },
        {
          from: resolve(__dirname, '..', 'node_modules/three/examples/jsm/loaders/ifc'),
          to: resolve(__dirname, '..', 'dist', 'ifc')
        },
        {
          from: resolve(__dirname, 'src/img'),
          to: resolve(__dirname, '..', 'dist')
        }
      ]
    })
  ],
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
    alias: {
      '@utils': resolve(__dirname, 'src/utils')
    }
  }
}
