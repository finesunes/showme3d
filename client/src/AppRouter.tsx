import React, { FC } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import NotFound from './pages/NotFound'
import Workspace from './pages/Workspace'
import About from './pages/About'

const AppRouter: FC = () => {
  return <Router>
    <Switch>
      <Route exact path="/" component={ Workspace } />
      <Route exact path="/about" component={ About } />
      <Route exact path="*" component={ NotFound } />
    </Switch>
   </Router>
}

export default AppRouter
