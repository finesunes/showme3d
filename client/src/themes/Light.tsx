// eslint-disable-next-line no-unused-vars
import React from 'react'
import { createTheme } from '@mui/material/styles'

declare module '@mui/material/styles' {
  export interface Theme extends Record<string, any> {}
  // eslint-disable-next-line no-unused-vars
  interface ThemeOptions {

  }
}

export default function createMyTheme () {
  return createTheme({
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          html: {
            height: '100%'
          },
          body: {
            margin: 0,
            height: '100%',
            padding: 'env(safe-area-inset-top) env(safe-area-inset-right) env(safe-area-inset-bottom) env(safe-area-inset-left)',
            fontFamily: 'Univia Pro Regular'
          }
        }
      },
      MuiAppBar: {

      },
      MuiButton: {
      }
    },
    palette: {
      // mode: 'dark',
      primary: {
        // light: will be calculated from palette.primary.main,
        main: '#5c5ea3'
        // dark: will be calculated from palette.primary.main,
        // contrastText: will be calculated to contrast with palette.primary.main
      },
      secondary: {
        main: '#ed6c02'
      }
    }
  })
}
