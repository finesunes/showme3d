import React from 'react'
import {
  AppBar, Container, IconButton,
  Toolbar, Typography
} from '@mui/material'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import LogoImg from '../../components/Button/LogoImg'

import { modelExtension } from '@utils/file'
import { styled } from '@mui/system'
import AttachFileIcon from '@mui/icons-material/AttachFile'
import LinkIcon from '@mui/icons-material/Link'
import IosShareIcon from '@mui/icons-material/IosShare'
import PanToolIcon from '@mui/icons-material/PanTool'
import OpenWithOutlinedIcon from '@mui/icons-material/OpenWithOutlined'
import ThreeDRotationIcon from '@mui/icons-material/ThreeDRotation'
import OpenInFullOutlinedIcon from '@mui/icons-material/OpenInFullOutlined'
import CachedIcon from '@mui/icons-material/Cached'
import LightModeIcon from '@mui/icons-material/LightMode'
import GridOnIcon from '@mui/icons-material/GridOn'
import CropSquareSharpIcon from '@mui/icons-material/CropSquareSharp'
import MenuIcon from '@mui/icons-material/Menu'

const Root = styled('div')`
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    border-radius: 4px;
  }

  td,
  th {
    border: 1px solid #ddd;
    text-align: left;
    padding: 2px;
  }

  th {
    background-color: #ddd;
  }
`

const About: React.FC = () => {
  const onArrowClick = () => {
    window.close()
  }

  return <>
    <AppBar position='relative' elevation={ 0 } color="primary" sx={ { height: '48px' } }>
      <Toolbar variant='dense'>
        <LogoImg src='/logo.svg' alt='logo' />
        <IconButton onClick={ onArrowClick } sx={ { color: '#FFF' } }>
          <ArrowBackIcon/>
        </IconButton>
      </Toolbar>
    </AppBar>
    <Container sx={ { overflow: 'auto' } } maxWidth="sm">
      <Typography sx={ { mt: 4 } } variant='h3'>Show my 3D</Typography>
      <Typography variant='body1'>
        It is a site for viewing and sharing one or more files in 3D format.
        You can also start animation of models here.
      </Typography>

      <Typography sx={ { mt: 4 } } variant='h3'>Supported file formats</Typography>
      <Root sx={ { width: '100px' } }>
        <table>
          <thead>
          <tr>
            <th>Extension</th>
          </tr>
          </thead>
          <tbody>
          { Object.values(modelExtension).map((extension, i) => <tr key={ i }><td>{ extension }</td></tr>) }
          </tbody>
        </table>
      </Root>

      <Typography sx={ { mt: 4 } } variant='h3'>Loading files</Typography>
      <Typography variant='body1'>
        You can upload files from your computer or from a remote server.
        Some models require multiple files to display correctly.
        You can also upload files as an archive.
      </Typography>
      <Typography variant='body1'>
        For example OBJ models MTL is required. <br />
        In this case, when downloading, select all the necessary files at once.
      </Typography>

      <Typography sx={ { mt: 2 } } variant='h4'>Loading models from your computer</Typography>
      <Typography variant='body1'>
        Click on &quot;<AttachFileIcon color='primary'/>&quot; and select files
      </Typography>

      <Typography sx={ { mt: 2 } } variant='h4'>Loading models hosted on a web server</Typography>
      <Typography variant='body1'>
        Click on the &quot;<LinkIcon color='primary'/>&quot; and paste the links to your files.
      </Typography>

      <Typography sx={ { mt: 2 } } variant='h4'>Loading models hosted on GitHub</Typography>
      <Typography variant='body1'>
        1. Commit models to GitHub with all the required files. <br/>
        2. Go to website, and click on the &quot;<LinkIcon color='primary'/>&quot; on the toolbar. <br/>
        3. Open the files on GitHub, copy the link of the file from the address bar,
        and paste it in the dialog. See the example below. <br/>
        (example) https://github.com/finesunes/sm3d_example_model/blob/main/Samba%20Dancing%20(1).fbx <br/>
        4. Click on &quot;Upload and open&quot;.
      </Typography>

      <Typography sx={ { mt: 2 } } variant='h4'>Loading models hosted on DropBox</Typography>
      <Typography variant='body1'>
        1. Upload models to DropBox with all the required files. <br/>
        2. Go to website, and click on the &quot;<LinkIcon color='primary'/>&quot; on the toolbar. <br/>
        3. Get the sharing link from DropBox for all of the files, and paste it in the dialog.<br/>
        4. Click on &quot;Upload and open&quot;.
      </Typography>

      <Typography sx={ { mt: 4 } } variant='h3'>Sharing models</Typography>
      <Typography variant='body1'>
        If your models are hosted on a web server (GitHub, DropBox), you can share the link with others <br/>
        Click on &quot;<IosShareIcon color='primary'/>&quot; and copy url in dialog.
      </Typography>

      <Typography sx={ { mt: 4 } } variant='h3'>Animations</Typography>
      <Typography variant='body1'>
        1. Upload your models. <br/>
        2. Click on the &quot;animations&quot; toggle switch in the scene settings.<br/>
        3. Go to the models tab and select the model you want.<br/>
        4. Open the animation list and run the required one.<br/>
      </Typography>

      <Typography sx={ { mt: 4 } } variant='h3'>Controls</Typography>
      <Root sx={ { width: '300px' } }>
        <table>
          <thead>
          <tr>
            <th>Button</th>
            <th>Description</th>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td><MenuIcon /></td>
              <td>Show / hide menu</td>
            </tr>
            <tr>
              <td><AttachFileIcon/></td>
              <td>File attaching</td>
            </tr>
            <tr>
              <td><LinkIcon/></td>
              <td>Uploading by url</td>
            </tr>
            <tr>
              <td><IosShareIcon /></td>
              <td>Sharing</td>
            </tr>
            <tr>
              <td><CachedIcon/></td>
              <td>Reload and clear workspace dialog</td>
            </tr>
            <tr>
              <td><PanToolIcon/></td>
              <td>Model dragging</td>
            </tr>
            <tr>
              <td><OpenWithOutlinedIcon/></td>
              <td>Model translate</td>
            </tr>
            <tr>
              <td><ThreeDRotationIcon/></td>
              <td>Model rotate</td>
            </tr>
            <tr>
              <td><OpenInFullOutlinedIcon/></td>
              <td>Model scaling</td>
            </tr>
            <tr>
              <td><LightModeIcon /></td>
              <td>Enable/disable camera light</td>
            </tr>
            <tr>
              <td>Ambient<LightModeIcon sx={ { ml: 1 } }/></td>
              <td>Enable/disable ambient light</td>
            </tr>
            <tr>
              <td><GridOnIcon /></td>
              <td>Enable/disable scene grid</td>
            </tr>
            <tr>
              <td><CropSquareSharpIcon/></td>
              <td>Enable/disable scene plane</td>
            </tr>
          </tbody>
        </table>
      </Root>

      <Typography color='primary' sx={ { mt: 4 } } variant='h6'>
        If you find errors or want to contact, write to me. Please record a video or take screenshots.
      </Typography>
      <Typography variant='h5'>My email: jira2broll@gmail.com</Typography>
      <Typography sx={ { pb: 10 } } variant='h5'>My telegram: <a href='https://t.me/justshiza' target='_blank' rel="noreferrer">@JustShiza</a></Typography>
    </Container>
  </>
}

export default About
