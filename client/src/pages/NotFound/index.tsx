import React, { FC } from 'react'

const NotFound: FC = () => {
  return <div>404 page not found!</div>
}

export default NotFound
