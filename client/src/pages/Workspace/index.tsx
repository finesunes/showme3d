import React, { FC } from 'react'

import WorkspaceCanvas from '../../components/Workspace'

const Workspace: FC = () => {
  return <>
    <WorkspaceCanvas />
  </>
}

export default Workspace
