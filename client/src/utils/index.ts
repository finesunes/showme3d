export const createDuplicateArrayFromValues = <T>(values: T[], count: number): T[] => {
  let newArray = Array.from(new Array(count))
  newArray = newArray.map((_, i) => values[((i + 1) % values.length)])
  return newArray
}

export type ThreeDimensionalCoordinates = {
  x: number,
  y: number,
  z: number,
}

export type ModelSettings = {
  animation?: string
  position?: ThreeDimensionalCoordinates
  scale?: ThreeDimensionalCoordinates
  rotation?: ThreeDimensionalCoordinates
}

export enum AttachMapEnum {
  specularMap = 'specularMap',
  bumpMap = 'bumpMap',
  normalMap = 'normalMap',
  texture = 'texture',
}

export type AttachMapsToMeshType = {
  [AttachMapEnum.texture]?: File | null,
  [AttachMapEnum.specularMap]?: File | null,
  [AttachMapEnum.bumpMap]?: File | null,
  [AttachMapEnum.normalMap]?: File | null,
}
