import { unarchiveFiles } from '@utils/unarchive'
import { toast } from 'react-toastify'
import Workspace from 'src/three/workspace'
import { LoadingData } from '../three/FileLoader'
import { loadAndUnarchive, transformUrl } from '@utils/url'

export enum modelExtension {
  ply = 'ply',
  stl = 'stl',
  gltf = 'gltf',
  glb = 'glb',
  obj = 'obj',
  fbx = 'fbx',
  rhino3dm = '3dm',
  tds3ds = '3ds',
  threeMF = '3mf',
  amf = 'amf',
  pcd = 'pcd',
  pmd = 'pmd',
  pmx = 'pmx',
  prwm = 'prwm',
  gcode = 'gcode',
  ifc = 'ifc',
  vox = 'vox',
  titl = 'tilt'
}

export const imgAvailableExt: string[] = ['png', 'jpg', 'jfif', 'jpe', 'jpeg', 'jp2', 'jpx', 'heif']
export const modelAvailableExt: string[] = Object.values(modelExtension)
export const archiveAvailableExt: string[] = ['zip', 'bz2', '7z', 'rar', 'gz', 'tar', 'tgz']

export const mtlExt: string = 'mtl'

export const getExt = (name: string): string => {
  const separatedName = name.trim().split('.')

  if (separatedName.length <= 1) {
    return ''
  }

  const ext = separatedName.pop()
  return ext ? ext.toLowerCase() : ''
}

export const testFileIsImg = (file: File): boolean => imgAvailableExt.indexOf(getExt(file.name)) >= 0

export const testFileIsModel = (file: File): boolean => modelAvailableExt.indexOf(getExt(file.name)) >= 0

export const testFileIsArchive = (file: File): boolean => archiveAvailableExt.indexOf(getExt(file.name)) >= 0

export const testFileIsMtl = (file: File): boolean => getExt(file.name) === mtlExt

export const loadingLocalFiles = async (
  files: File[],
  workspace: Workspace,
  callbackFunc: (v: boolean) => any
): Promise<void> => {
  try {
    callbackFunc(true)

    const archiveFiles = await unarchiveFiles(files.filter(testFileIsArchive), { onlyFiles: true })

    toast.promise(workspace.loadFiles(archiveFiles.concat(files.filter(file => !testFileIsArchive(file)))), {
      pending: 'Loading model',
      success: 'Model is loaded',
      error: 'Error. Model is not loaded'
    }, { toastId: 'workspace' })
      .then(() => callbackFunc(false))
      .catch(() => callbackFunc(false))
  } catch (error) {
    callbackFunc(false)
  }
}

export const loadingByUrlFiles = async (
  modelsUrls: string[],
  archivesUrls: string[],
  workspace: Workspace,
  callbackFunc: (v: boolean) => any
): Promise<void> => {
  try {
    callbackFunc(true)

    const unarchivedFiles = await Promise.all(archivesUrls.map(archiveUrl => loadAndUnarchive([archiveUrl])))

    const loadingArchivesData: LoadingData[] = unarchivedFiles.map((files, i) => {
      return files.map(file => ({
        archiveUrl: archivesUrls[i],
        src: file,
        settings: {}
      }))
    }).flat()

    toast.promise(workspace.loadFiles(
      ([] as (LoadingData | string)[]).concat(loadingArchivesData || []).concat(modelsUrls.map(transformUrl))
    ),
    {
      pending: 'Loading model',
      success: 'Model is loaded',
      error: 'Error. Model is not loaded'
    }, { toastId: 'workspace' })
      .then(() => callbackFunc(false))
      .catch(() => callbackFunc(false))
  } catch (error) {
    toast.error(error)
    callbackFunc(false)
  }
}

// TODO 'mtl'
