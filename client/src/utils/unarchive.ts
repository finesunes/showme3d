import { Archive } from 'libarchive.js/main'
import { toast } from 'react-toastify'

Archive.init({ workerUrl: '/libarchive/worker-bundle.js' })

type Options = {
  onlyFiles?: boolean,
  compressed?: boolean,
}

const unarchive = async (file: File, options: Options = { onlyFiles: false, compressed: false }) => {
  const { onlyFiles, compressed } = options

  const archive = await Archive.open(file)

  if (!compressed) {
    await archive.extractFiles()
  }

  if (onlyFiles) {
    return (await archive.getFilesArray()).map(v => v.file)
  }

  return await archive.getFilesObject()
}

const unarchiveFiles = async (
  files: File[], options: Options = { onlyFiles: false, compressed: false }
): Promise<File[]> => {
  if (!files.length) return []

  return (await toast.promise(
    Promise.allSettled(files.map(archive => unarchive(archive, options))),
    {
      pending: 'Unzipping files',
      success: 'Files unzipped',
      error: 'Unzip files failed'
    }
  )).reduce((memo, v) => {
    if (v.status === 'fulfilled' && v.value) {
      return memo.concat(v.value as File[])
    }

    return memo
  }, [] as File[])
}

export {
  unarchive, unarchiveFiles
}
