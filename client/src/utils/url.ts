import { toast } from 'react-toastify'
import URI from 'urijs'
import { unarchiveFiles } from './unarchive'

enum UrlSearchKeys {
  model = 'model',
  archive = 'archive'
}

const transformUrl = (value: string): string => {
  const url = new URI(value)

  switch (url.domain()) {
    case 'github.com':
      return url
        .domain('githubusercontent.com')
        .subdomain('raw')
        .segment(2, '') // remove "blob"
        .toString()
    case 'dropbox.com':
      return url.hostname('dl.dropbox.com').search('').toString()
    default:
      return value
  }
}

const transformUrls = (urls: string[]): string[] => {
  if (!urls) return []

  return urls.map(transformUrl)
}

const responseToFile = (response: Response, filename: string): Promise<File> => new Promise<File>((resolve, reject) => {
  response.blob()
    .then((blob) => resolve(new File([blob], filename)))
    .catch(err => reject(err))
})

const loadFilesByUrls = async (urls: string[]) => {
  if (!urls?.length) return [] as File[]

  const toastId = toast.loading('Archive loading...')

  const preparedUrls = transformUrls(urls)

  const validResponses = (await Promise.allSettled(preparedUrls.map((url: string) => fetch(url))))
    .reduce((memo, res) => {
      if (res.status === 'fulfilled' && res.value) {
        memo.push(res.value)
      }

      return memo
    }, [] as Response[])

  const files = await Promise.all(
    validResponses
      .filter(Boolean)
      .map((response, i) => responseToFile(response, `archive-${i}`))
  )

  toast.update(toastId, { render: 'Archive loaded!', type: 'success', isLoading: false, autoClose: 3000 })

  return files
}

const loadAndUnarchive = async (urls: string[]): Promise<File[]> => {
  if (!urls?.length) return [] as File[]

  try {
    return unarchiveFiles(await loadFilesByUrls(urls), { onlyFiles: true })
  } catch (e) {
    console.log(e)
    return []
  }
}

const modifyQueryWrapper = (history: { location: { pathname: any; search: any }; push: any }) =>
  (property: string, value: string | null | number) => {
    const { location: { pathname, search }, push } = history

    const newSearch = new URI().search(search).search(data => {
      if (value !== null) {
        data[property] = value
      } else {
        delete data[property]
      }

      return data
    })

    push({ pathname, search: newSearch.search() })
  }

const getValueFromQuery = (propName?: string, options?: { arrayAsString?: boolean }) => {
  if (!propName) return null

  const propValue = new URI(window.location.href).search(true)[propName]

  if (Array.isArray(propValue) && options?.arrayAsString) {
    return propValue.join('\n')
  }

  return propValue
}

export {
  UrlSearchKeys,
  transformUrl,
  loadAndUnarchive,
  modifyQueryWrapper,
  getValueFromQuery
}
