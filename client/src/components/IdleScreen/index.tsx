import React, { useContext, useMemo, useState } from 'react'
import { Box, BoxProps, Button, IconButton, Typography } from '@mui/material'
import styled from '@mui/system/styled'
import { WorkspaceContext } from '../../context/WorkspaceContext'

import { modelExtension } from '@utils/file'
import AttachFileIcon from '@mui/icons-material/AttachFile'
import Link from '../Link'
import AddFiles from '../Button/AddFiles'
import AddFilesByUrl from '../Button/AddFilesByUrl'

type IdleScreenProps = BoxProps & {
  open?: boolean
}

const StyledIdleScreen = styled(Box, { shouldForwardProp: propName => propName !== 'open' })<IdleScreenProps>(({ theme, open }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  display: open ? 'flex' : 'none',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  // zIndex: 1000,
  background: '#FFF',
  opacity: 0.6,
  padding: theme.spacing(4)
}))

const IdleScreen: React.FC = (props) => {
  const [idleOpen, setIdleOpen] = useState(true)

  const extensions = useMemo(() => Object.values(modelExtension).join(', '), [])

  const { workspace, loading } = useContext(WorkspaceContext)

  if (!workspace) {
    return <></>
  }

  const onClose = () => setIdleOpen(false)

  return <StyledIdleScreen { ...props } open={ (idleOpen && !loading && !workspace.models.length) }>
    <Typography color='secondary' variant='h4'>
      Online 3D model and animation viewer.
    </Typography>
    <Typography variant='h5'>
      Download file from your computer (tap on <AddFiles component={ IconButton } aria-label='Add local files'><AttachFileIcon/></AddFiles>) or drag and drop your models here.
    </Typography>
    <Typography variant='h5'>
      Download file from link (tap on <AddFilesByUrl component={ IconButton } aria-label='Add files by url' />).
    </Typography>
    <Typography variant='h6'>
      To find out information about the application and its management, click on the logo or <Link target='_blank' to='/about' > here </Link>
    </Typography>
    <Typography variant='body1'>
      Supported file formats:
      {
        extensions
      }
    </Typography>
    <Button sx={ { mt: 3 } } onClick={ onClose }><Typography variant='h5'>Close description</Typography></Button>
  </StyledIdleScreen>
}

export default React.memo(IdleScreen)
