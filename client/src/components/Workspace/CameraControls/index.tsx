import React, { useMemo } from 'react'
import { List, ListItem } from '@mui/material'
import { ControlType } from '../../../three/workspace/CameraFacade'
import ToggleGroup from '../../Togglers/ToggleGroup'
import { WorkspaceContext } from '../../../context/WorkspaceContext'

import OpenInFullOutlinedIcon from '@mui/icons-material/OpenInFullOutlined'
import PanToolIcon from '@mui/icons-material/PanTool'
import OpenWithOutlinedIcon from '@mui/icons-material/OpenWithOutlined'
import ThreeDRotationIcon from '@mui/icons-material/ThreeDRotation'

type ToggleDataType = {
  name: string
  enableFunc(): any
  children: React.ReactNode,
  ariaLabel?: string
}

const CameraControls: React.FC = () => {
  const { workspace } = React.useContext(WorkspaceContext)

  return useMemo(() => {
    if (!workspace) {
      return <></>
    }

    const camera = workspace.camera

    const data: ToggleDataType[] = [
      {
        name: ControlType.drag,
        enableFunc: camera.enableDragControls,
        children: <PanToolIcon/>,
        ariaLabel: 'Dragging'
      },
      {
        name: ControlType.translate,
        enableFunc: camera.enableTransformControls,
        children: <OpenWithOutlinedIcon/>,
        ariaLabel: 'Translate'
      },
      {
        name: ControlType.rotate,
        enableFunc: () => camera.enableTransformControls(ControlType.rotate),
        children: <ThreeDRotationIcon/>,
        ariaLabel: 'Rotate'
      },
      {
        name: ControlType.scale,
        enableFunc: () => camera.enableTransformControls(ControlType.scale),
        children: <OpenInFullOutlinedIcon/>,
        ariaLabel: 'Scale'
      }
    ]

    return <List>
      <ListItem>
        <ToggleGroup data={ data } active={ camera.activeControl } disableFunc={ camera.disableAllControls }/>
      </ListItem>
    </List>
  }, [workspace])
}

export default CameraControls
