import React, { useContext, useState } from 'react'

import SceneControls from './SceneControls'
import CameraControls from './CameraControls'

import ModelsControls from './ModelsControls'
import { Box, Tab, Tabs } from '@mui/material'
import TabPanel from '../Tab/TabPanel'
import { styled } from '@mui/system'
import AppBar from '../AppBar'
import { WorkspaceContext } from '../../context/WorkspaceContext'

interface IWorkspaceDrawer {
  open: boolean,
}

const WorkspaceDrawer = styled(Box, {
  shouldForwardProp: (propName: PropertyKey) => propName !== 'open'
})<IWorkspaceDrawer>(({ theme }) => ({
  height: '100%',
  overflow: 'auto',
  gridArea: 'drawer',
  borderLeft: `1px solid ${theme.palette.primary.light}`,
  // display: open ? 'block' : 'none',
  [theme.breakpoints.down('md')]: {
    borderLeft: '0px solid transparent',
    position: 'absolute',
    width: '100vw',
    left: '-100vw',
    top: 0,
    backgroundColor: theme.palette.grey[50]
  }
}))

const Controls: React.FC = () => {
  const [tab, setTab] = useState<string>('scene')

  const { drawer } = useContext(WorkspaceContext)

  const onChangeTab = (_: React.SyntheticEvent, newTabValue: string): void => setTab(newTabValue)

  return <>
      <AppBar/>
      <WorkspaceDrawer hidden={ !drawer } open={ drawer }>
        <Tabs value={ tab } onChange={ onChangeTab } variant={ 'fullWidth' }>
          <Tab value={ 'scene' } label={ 'Scene' } />
          <Tab value={ 'models' } label={ 'Models' } />
        </Tabs>
        <TabPanel index={ tab } value={ 'scene' } sx={ { pt: 1 } }>
          <CameraControls />
          <SceneControls />
        </TabPanel>
        <TabPanel index={ tab } value={ 'models' } sx={ { pt: 1 } }>
          <ModelsControls />
        </TabPanel>
      </WorkspaceDrawer>
  </>
}

export default React.memo(Controls)
