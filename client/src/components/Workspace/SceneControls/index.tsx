import React from 'react'

import SliderListItem from '../../List/ListItem/SliderListItem'

import { List, ListItem } from '@mui/material'
import { WorkspaceContext } from '../../../context/WorkspaceContext'
import BackgroundTextureDialog from './BackgroundTextureDialog'
import BackgroundPlaneDialog from './BackgroundPlaneDialog'

import SceneToggleControl from './SceneToggleControl'

const SceneControls: React.FC = () => {
  const { workspace } = React.useContext(WorkspaceContext)
  if (!workspace) {
    return <></>
  }

  const scene = workspace.scene

  return <List>
    <ListItem sx={ { flexDirection: 'column', alignItems: 'flex-start' } }>
      <SceneToggleControl />
    </ListItem>
    <ListItem>
      <BackgroundTextureDialog/>
    </ListItem>
    {
      scene.isExistPlane &&
      <ListItem>
        <BackgroundPlaneDialog />
      </ListItem>
    }
    <List>
      {
        scene.isExistAmbientLight &&
        <SliderListItem initialValue={ scene.ambientLight?.intensity || 0 } min={ 0 } max={ 5 } step={ 0.1 } text='ambient intensity'
          onChangeFunc={ scene.setAmbientIntensity } />
      }
      {
        scene.isExistCameraLight &&
        <SliderListItem initialValue={ scene.cameraLight?.intensity || 0 } min={ 0 } max={ 10 } step={ 0.1 } text='camera light intensity'
          onChangeFunc={ scene.setCameraLightIntensity } />
      }
      {
        scene.isExistFog &&
        <SliderListItem initialValue={ scene.customFog?.far || 0 } min={ 0 } max={ 100 } step={ 1 } text='fog far'
          onChangeFunc={ scene.setFogFar } />
      }
    </List>
  </List>
}

export default SceneControls
