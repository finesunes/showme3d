import React, { useState } from 'react'

import { Button, IconButton, List, ListItem, ListItemText, TextField, Typography } from '@mui/material'
import FileUploadIcon from '@mui/icons-material/FileUpload'
import SelectFiles from '../../Button/SelectFiles'
import { Box } from '@mui/system'
import { toast } from 'react-toastify'
import DeleteIcon from '@mui/icons-material/Delete'
import { transformUrl } from '@utils/url'

type Props = {
  setImgFunc(file: File | null | string): Promise<void>
}

const SetImg: React.FC<Props> = ({ setImgFunc }) => {
  const [file, setFile] = useState<File | null>(null)
  const [url, setUrl] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(false)

  const onSelectFile = (files: File[]) => {
    const newFile = files[0]

    if (newFile) {
      setFile(newFile)
    } else {
      setFile(null)
    }
  }
  const handleChangeUrl = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    e.preventDefault()

    setUrl(e.target.value)
  }
  const onRemove = () => {
    setFile(null)
    setLoading(true)

    toast.promise(setImgFunc(null), {
      pending: 'Loading',
      success: 'Image removed',
      error: 'Error. Image is not removed'
    }, { toastId: 'workspace' }).then(() => setLoading(false)).catch(() => setLoading(false))
  }
  const onSaveBtn = () => {
    if (file || url) {
      setLoading(true)

      toast.promise(setImgFunc(file || transformUrl(url)), {
        pending: 'Img loading',
        success: 'Image is set',
        error: 'Error. Image is not set'
      }, { toastId: 'workspace' }).then(() => setLoading(false)).catch(() => {
        setLoading(false)
        setUrl('')

        setImgFunc(null)
      })
    } else {
      onRemove()
    }
  }

  return <>
    <TextField disabled={ !!file } value={ url } sx={ { mt: 1 } } onChange={ handleChangeUrl }
      label={ 'Input url' } fullWidth multiline variant={ 'outlined' } />
    <Box sx={ { display: 'flex', justifyContent: 'center' } }><Typography sx={ { m: 1 } }>Or</Typography></Box>
    <SelectFiles fullWidth color='primary' disabled={ !!url } options={ { multiple: false } } variant={ 'outlined' } onSelectFiles={ onSelectFile }>
      upload texture <FileUploadIcon/>
    </SelectFiles>
    {
      file?.name &&
      <List>
        <ListItem>
          <ListItemText primary={ `${file.name}` } />
          <IconButton onClick={ () => setFile(null) } color='error'>
            <DeleteIcon />
          </IconButton>
        </ListItem>
      </List>
    }
    <Box sx={ { display: 'flex', mt: 1 } }>
      <Button disabled={ loading } onClick={ onRemove } fullWidth>Remove</Button>
      <Button disabled={ loading } color='secondary' onClick={ onSaveBtn } fullWidth variant={ 'contained' } sx={ { mr: 1 } }>Set</Button>
    </Box>
  </>
}

export default SetImg
