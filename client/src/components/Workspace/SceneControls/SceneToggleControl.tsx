import React from 'react'
import Toggle from '../../Togglers/Toggle'
import GridOnIcon from '@mui/icons-material/GridOn'
import CropSquareSharpIcon from '@mui/icons-material/CropSquareSharp'
import LightModeIcon from '@mui/icons-material/LightMode'
import { ToggleButtonGroup } from '@mui/material'
import { WorkspaceContext } from '../../../context/WorkspaceContext'
import styled from '@emotion/styled'

const StyledToggleButtonGroup = styled(ToggleButtonGroup)(() => ({
  marginBottom: '8px',
  flexWrap: 'wrap'
}))

const SceneToggleControl: React.FC = () => {
  const { workspace, update } = React.useContext(WorkspaceContext)
  if (!workspace) {
    return <></>
  }

  const scene = workspace.scene

  const enableAnimations = () => {
    workspace.autoAnimate = true
  }
  const disableAnimations = () => {
    workspace.autoAnimate = false
  }

  return <>
    <StyledToggleButtonGroup size='small'>
      <Toggle aria-label='Fog toggle' value='' initialState={ scene.isExistFog } enableFunc={ scene.enableFog }
        disableFunc={ scene.disableFog } updateWorkspace={ update }
      >
        fog
      </Toggle>
      <Toggle aria-label='Camera light toggle' value='' initialState={ scene.isExistCameraLight } enableFunc={ scene.enableCameraLight }
        disableFunc={ scene.disableCameraLight } updateWorkspace={ update }
      >
        <LightModeIcon fontSize='small'/>
      </Toggle>
      <Toggle aria-label='Ambient light toggle' value='' initialState={ scene.isExistAmbientLight } enableFunc={ scene.enableAmbientLight }
        disableFunc={ scene.disableAmbientLight } updateWorkspace={ update }
      >
        ambient<LightModeIcon sx={ { ml: 1 } } fontSize='small'/>
      </Toggle>
    </StyledToggleButtonGroup>
    <StyledToggleButtonGroup size='small' >
      <Toggle aria-label='Grid toggle' value='' initialState={ scene.isExistGridHelper } enableFunc={ scene.enableGridHelper }
        disableFunc={ scene.disableGridHelper }
      >
        <GridOnIcon fontSize='small'/>
      </Toggle>
      <Toggle aria-label='Plane toggle' value='' initialState={ scene.isExistPlane } enableFunc={ scene.enablePlane }
        disableFunc={ scene.disablePlane } updateWorkspace={ update }
      >
        <CropSquareSharpIcon/>
      </Toggle>
      <Toggle aria-label='Axes toggle' value='' initialState={ scene.isExistAxesHelper } enableFunc={ scene.enableAxesHelper }
        disableFunc={ scene.disableAxesHelper }
      >
        axes
      </Toggle>
    </StyledToggleButtonGroup>
    <StyledToggleButtonGroup size='small'>
      <Toggle aria-label='Animations toggle' value='' initialState={ workspace.autoAnimate } enableFunc={ enableAnimations }
        disableFunc={ disableAnimations } updateWorkspace={ update }
      >
        Animations
      </Toggle>
    </StyledToggleButtonGroup>
  </>
}

export default SceneToggleControl
