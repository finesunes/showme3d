import React, { useState } from 'react'
import { ChromePicker, ColorResult } from 'react-color'

import { styled } from '@mui/system'
import { Button } from '@mui/material'
import { Color } from 'three'

import { toast } from 'react-toastify'

const StyledChromeColorPicker = styled(ChromePicker)(({
  boxShadow: 'transparent 0px 0px 0px !important',
  width: '100% !important'
}))

type Props = {
  setColorFunc(value: Color): Promise<void>;
}

const SetColor: React.FC<Props> = ({ setColorFunc }) => {
  const [color, setColor] = useState<string>('#FFF')
  const [loading, setLoading] = useState<boolean>(false)

  const onChangeColor = (color: ColorResult): void => {
    setColor(color.hex)
  }
  const onSaveBtn = () => {
    if (color) {
      setLoading(true)

      toast.promise(setColorFunc(new Color(color)), {
        pending: 'Loading',
        success: 'Color is set',
        error: 'Error. Color is not set'
      }, { toastId: 'workspace' }).then(() => setLoading(false)).catch(() => setLoading(false))
    }
  }

  return <>
    <StyledChromeColorPicker disableAlpha color={ color } onChange={ onChangeColor }/>
    <Button disabled={ loading } color='secondary' onClick={ onSaveBtn } fullWidth variant={ 'contained' } sx={ { pt: 1 } }>Set background</Button>
  </>
}

export default SetColor
