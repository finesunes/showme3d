import React, { useMemo, useState } from 'react'
import { WorkspaceContext } from '../../../context/WorkspaceContext'

import Dialog from '../../Dialog'
import {
  Button,
  DialogContent,
  Tab,
  Tabs
} from '@mui/material'

import TabPanel from '../../Tab/TabPanel'
import SetColor from './SetColor'
import SetImg from './SetImg'
import SliderListItem from '../../List/ListItem/SliderListItem'

enum TabEnum {
  main,
  img,
  color
}

type TabValue = TabEnum.img | TabEnum.color

const BackgroundPlaneDialog: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false)
  const [tab, setTab] = useState<TabValue>(TabEnum.color)

  const { workspace } = React.useContext(WorkspaceContext)
  if (!workspace) return <></>

  const onClose = () => setOpen(false)
  const onBtnClick = () => setOpen(true)
  const onChangeTab = (_: React.SyntheticEvent, newTabValue: TabValue) => setTab(newTabValue)

  return useMemo(() => {
    return <>
      <Button fullWidth color='primary' variant={ 'contained' } onClick={ onBtnClick }>Plane settings</Button>
      <Dialog open={ open } onClose={ onClose } title='Change background'>
        <DialogContent>
          <Tabs value={ tab } onChange={ onChangeTab } centered>
            <Tab value={ TabEnum.main } label={ 'size' } />
            <Tab value={ TabEnum.img } label={ 'image' } />
            <Tab value={ TabEnum.color } label={ 'color' } />
          </Tabs>
          <TabPanel index={ tab } value={ TabEnum.main } sx={ { pt: 1 } }>
            <SliderListItem initialValue={ workspace.scene.planeScalar } step={ 1 } min={ 1 } max={ 100 } text='plane size'
              onChangeFunc={ (value) => { workspace.scene.planeScalar = value } } />
          </TabPanel>
          <TabPanel index={ tab } value={ TabEnum.img } sx={ { pt: 1 } }>
            <SetImg setImgFunc={ workspace.scene.setPlaneBackground } />
          </TabPanel>
          <TabPanel index={ tab } value={ TabEnum.color } sx={ { pt: 1 } }>
            <SetColor setColorFunc={ workspace.scene.setPlaneBackground } />
          </TabPanel>
        </DialogContent>
      </Dialog>
    </>
  }, [workspace, tab, open])
}

export default BackgroundPlaneDialog
