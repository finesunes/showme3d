import React, { useMemo, useState } from 'react'
import { WorkspaceContext } from '../../../context/WorkspaceContext'

import Dialog from '../../Dialog'
import SetBackgroundCubeImg from './SetBackgroundCubeImg'
import {
  Button,
  DialogContent,
  Tab,
  Tabs
} from '@mui/material'

import TabPanel from '../../Tab/TabPanel'
import SetColor from './SetColor'
import SetImg from './SetImg'

enum TabEnum {
  cube,
  color,
  img
}

type TabValue = TabEnum.cube | TabEnum.color | TabEnum.img

const BackgroundTextureDialog: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false)
  const [tab, setTab] = useState<TabValue>(TabEnum.color)

  const { workspace } = React.useContext(WorkspaceContext)
  if (!workspace) return <></>

  const onClose = () => setOpen(false)
  const onBtnClick = () => setOpen(true)
  const onChangeTab = (_: React.SyntheticEvent, newTabValue: TabValue) => setTab(newTabValue)

  return useMemo(() => {
    return <>
      <Button fullWidth color='primary' variant={ 'contained' } onClick={ onBtnClick }>Background settings</Button>
      <Dialog open={ open } onClose={ onClose } title='Change background'>
        <DialogContent>
          <Tabs value={ tab } onChange={ onChangeTab } centered>
            <Tab value={ TabEnum.img } label={ 'img' } />
            <Tab value={ TabEnum.cube } label={ 'cube img' } />
            <Tab value={ TabEnum.color } label={ 'color' } />
          </Tabs>
          <TabPanel index={ tab } value={ TabEnum.img } sx={ { pt: 1 } }>
            <SetImg setImgFunc={ workspace.scene.setBackground } />
          </TabPanel>
          <TabPanel index={ tab } value={ TabEnum.cube } sx={ { pt: 1 } }>
            <SetBackgroundCubeImg/>
          </TabPanel>
          <TabPanel index={ tab } value={ TabEnum.color } sx={ { pt: 1 } }>
            <SetColor setColorFunc={ workspace.scene.setBackground } />
          </TabPanel>
        </DialogContent>
      </Dialog>
    </>
  }, [workspace, open, tab])
}

export default BackgroundTextureDialog
