import React, { useState } from 'react'
import { createDuplicateArrayFromValues } from '@utils'
import SelectFiles from '../../Button/SelectFiles'
import FileUploadIcon from '@mui/icons-material/FileUpload'
import { Button, FormControl, InputLabel, List, ListItem, NativeSelect } from '@mui/material'
import map from 'lodash/map'
import { WorkspaceContext } from '../../../context/WorkspaceContext'
import { toast } from 'react-toastify'

export enum BackgroundSide {
  left='left',
  right='right',
  top='top',
  bottom='bottom',
  front='front',
  back='back',
}

export type orderedFilesBySides = {
  [BackgroundSide.left]: File
  [BackgroundSide.right]: File
  [BackgroundSide.top]: File
  [BackgroundSide.bottom]: File
  [BackgroundSide.front]: File
  [BackgroundSide.back]: File
}

const SetBackgroundCubeImg: React.FC = () => {
  const [files, setFiles] = useState<File[]>([])
  const [backgroundLoading, setBackgroundLoading] = useState<boolean>(false)
  const [orderedFiles, setOrderedFiles] = useState<orderedFilesBySides | null>(null)

  const { workspace } = React.useContext(WorkspaceContext)

  if (!workspace) return <></>

  const onSelectFiles = (newFiles: File[]): void => {
    const duplicatedArr = createDuplicateArrayFromValues(newFiles, 6)

    const orderedFiles: orderedFilesBySides = {
      [BackgroundSide.left]: duplicatedArr[0],
      [BackgroundSide.right]: duplicatedArr[1],
      [BackgroundSide.top]: duplicatedArr[2],
      [BackgroundSide.bottom]: duplicatedArr[3],
      [BackgroundSide.front]: duplicatedArr[4],
      [BackgroundSide.back]: duplicatedArr[5]
    }

    setFiles(duplicatedArr)
    setOrderedFiles(orderedFiles)
  }
  const onChangeOrder = (side: BackgroundSide) => (e: React.ChangeEvent<HTMLSelectElement>): void => {
    const selectedFile = files.find(v => v.name === e.target.value)
    if (selectedFile && orderedFiles) {
      orderedFiles[side] = selectedFile
      setOrderedFiles(orderedFiles)
    }
  }
  const onSaveBtn = async () => {
    if (orderedFiles) {
      setBackgroundLoading(true)

      toast.promise(workspace.scene.setBackground(Object.values(orderedFiles)), {
        pending: 'Cube img loading',
        success: 'Cube image is set',
        error: 'Error. Cube image is not set'
      }, { toastId: 'workspace' })
        .then(() => setBackgroundLoading(false))
        .catch(() => setBackgroundLoading(false))
    }
  }

  return <>
    <SelectFiles onSelectFiles={ onSelectFiles } fullWidth color='primary' variant={ 'outlined' } >
      upload texture <FileUploadIcon/>
    </SelectFiles>
    {
      !!files.length &&
      <List>
        {
          map(orderedFiles, (file, side) => <ListItem key={ side }>
            <FormControl fullWidth>
              <InputLabel variant="standard" htmlFor="uncontrolled-native">
                { side }
              </InputLabel>
              <NativeSelect
                defaultValue={ file.name }
                onChange={ onChangeOrder(side as BackgroundSide) }
                inputProps={ {
                  name: 'age',
                  id: 'uncontrolled-native'
                } }
              >
                { files.map((file, fileIdx) =>
                  <option key={ fileIdx } value={ file?.name }>{ file?.name }</option>) }
              </NativeSelect>
            </FormControl>
          </ListItem>)
        }
      </List>
    }
    {
      !!orderedFiles &&
        <Button fullWidth variant={ 'contained' } disabled={ backgroundLoading } onClick={ onSaveBtn }>Set background</Button>
    }
  </>
}

export default SetBackgroundCubeImg
