import { styled } from '@mui/system'
import { Box } from '@mui/material'

type DropZoneProps = {
  isDragging?: boolean
}

const CanvasContainer = styled(Box, { shouldForwardProp: propName => propName !== 'isDragging' })<DropZoneProps>(
  () => ({
    height: '100%',
    width: '100%',
    gridArea: 'workspace',
    position: 'relative',
    outlineWidth: '0px !important',
    userSelect: 'none'
  })
)

export default CanvasContainer
