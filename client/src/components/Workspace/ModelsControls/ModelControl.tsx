import React from 'react'
import { ModelDataType } from '../../../three/workspace/Models'
import SliderListItem from '../../List/ListItem/SliderListItem'
import CollapseList from '../../List/CollapseList'
import { Button, List, ListItem, ListItemText, Typography } from '@mui/material'
import SettingsIcon from '@mui/icons-material/Settings'
import MeshAnimationControl from './MeshAnimationControl'
import { styled } from '@mui/system'
import { WorkspaceContext } from '../../../context/WorkspaceContext'

const MeshesList = styled(List)(({ theme }) => ({
  marginBottom: theme.spacing(1.5),
  boxShadow: `0 0 6px ${theme.palette.secondary.light}`,
  borderRadius: theme.spacing(0.5),
  '& > .MuiListItem-root': {
    cursor: 'pointer',
    justifyContent: 'space-between'
  }
}))

type Props = {
  modelData: ModelDataType;
  modelUuid: string;
  openMeshSetting(uuid: string, meshName: string): void;
}

const ModelControl: React.FC<Props> = ({ modelData, modelUuid, openMeshSetting }) => {
  const { workspace, update } = React.useContext(WorkspaceContext)

  if (!workspace) {
    return <></>
  }

  const onReset = (modelUuid: string) => () => {
    workspace.models.resetModelByUuid(modelUuid)
    update()
  }
  const onDeleteModel = (modelUuid: string) => () => {
    workspace.models.removeModelByUuid(modelUuid)
    update()
  }

  return <>
    <SliderListItem initialValue={ 1 } min={ 0 } max={ 10 } step={ 0.1 }
      text={ 'Scale' }
      onChangeFunc={ value => workspace.models.setScaleModel(modelUuid, value) }
    />
    <CollapseList buttonText='Meshes' sx={ { pl: 1, pr: 1 } }>
      <MeshesList>
        {
          modelData.meshes.map((mesh, idx) => <ListItem key={ mesh.uuid }>
            <ListItemText>{ mesh.name || `_${idx}` }</ListItemText>
            <Button onClick={ () => openMeshSetting(mesh.uuid, mesh.name) }><SettingsIcon fontSize='small'/></Button>
          </ListItem>)
        }
      </MeshesList>
    </CollapseList>
    {
      !!modelData.animationsData.length &&
      <CollapseList buttonText='Animations' sx={ { pl: 1, pr: 1 } }>
        {
          !workspace.autoAnimate &&
          <ListItem><Typography>To play animations enable them in the scene settings!</Typography></ListItem>
        }
        {
          workspace.autoAnimate &&
          modelData.animationsData.map((animationData) =>
            <MeshAnimationControl key={ animationData.uuid } animations={ animationData.animations } />
          )
        }
      </CollapseList>
    }
    <ListItem>
      <Button fullWidth onClick={ onDeleteModel(modelUuid) }>Remove model</Button>
      <Button fullWidth variant={ 'contained' } color='secondary' onClick={ onReset(modelUuid) }>Reset</Button>
    </ListItem>
  </>
}

export default React.memo(ModelControl)
