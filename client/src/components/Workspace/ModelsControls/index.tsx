import React, { useState } from 'react'
import { DialogContent } from '@mui/material'

import map from 'lodash/map'
import CollapseList from '../../List/CollapseList'
import Toggle from '../../Togglers/Toggle'
import ModelControl from './ModelControl'
import { WorkspaceContext } from '../../../context/WorkspaceContext'
import PrimaryDialog from '../../Dialog'
import SetMapToMesh from './SetMapToMesh'

const ModelsControls: React.FC = () => {
  const [selectedUuid, setSelectedUuid] = useState<{uuid: string, name: string} | null>(null)

  const { workspace } = React.useContext(WorkspaceContext)
  if (!workspace) {
    return <></>
  }

  const models = workspace.models

  if (!models.length) {
    return <></>
  }

  const openDialog = (uuid: string, meshName: string) => {
    setSelectedUuid({ uuid, name: meshName })
  }
  const closeDialog = () => {
    setSelectedUuid(null)
  }

  return <>
    <Toggle value=''
      enableFunc={ () => models.setVisibleSkeletonHelper(true) }
      disableFunc={ () => models.setVisibleSkeletonHelper(false) }
      initialState={ false }
      size='small'
      fullWidth
    >
      Skeleton
    </Toggle>
    {
      map(models.getAllModelsData(), (data, uuid) =>
        <CollapseList key={ uuid } buttonText={ `${data.name}` } sx={ { p: 0, borderBottom: '1px solid #000' } }>
          <ModelControl modelData={ data }
            modelUuid={ uuid }
            openMeshSetting={ openDialog }
          />
        </CollapseList>
      )
    }
    <PrimaryDialog open={ !!selectedUuid } onClose={ closeDialog } title={ `${selectedUuid?.name} mesh settings` }>
      <DialogContent>
        {
          selectedUuid &&
          <SetMapToMesh uuid={ selectedUuid.uuid }/>
        }
      </DialogContent>
    </PrimaryDialog>
  </>
}

export default ModelsControls
