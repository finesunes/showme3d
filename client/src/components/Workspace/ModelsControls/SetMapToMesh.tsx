import React, { useState } from 'react'

import { AttachMapEnum, AttachMapsToMeshType } from '@utils'
import { Button, List, ListItem, ListItemText } from '@mui/material'
import SelectFiles from '../../Button/SelectFiles'
import FileUploadIcon from '@mui/icons-material/FileUpload'
import DeleteIcon from '@mui/icons-material/Delete'
import { WorkspaceContext } from '../../../context/WorkspaceContext'

type Props = {
  uuid: string,
}

const SetMapToMesh: React.FC<Props> = ({ uuid }) => {
  const [selectedFiles, setSelectedFiles] = useState<AttachMapsToMeshType>({})
  const [loading, setLoading] = useState<boolean>(false)

  const { workspace } = React.useContext(WorkspaceContext)

  if (!workspace) return <></>

  const onSelectFile = (attachType: AttachMapEnum) => (files: File[]) => {
    const file = files[0]
    setSelectedFiles({ ...selectedFiles, [attachType]: file })
  }
  const onDeleteBtn = (attachType: AttachMapEnum) => () => {
    setSelectedFiles({ ...selectedFiles, [attachType]: null })
  }
  const onSaveBtn = () => {
    setLoading(true)
    workspace.models.attachMapToMesh(uuid, selectedFiles).then(() => setLoading(false)).catch(() => setLoading(false))
  }

  return <>
    <List>
      {
        Object.values(AttachMapEnum).map(attachType => <React.Fragment key={ attachType }>
          <ListItem>
            <ListItemText primary={ attachType } />
            {
              selectedFiles?.[attachType]
                ? <Button color='error' onClick={ onDeleteBtn(attachType) }><DeleteIcon/></Button>
                : <SelectFiles options={ { multiple: false } } onSelectFiles={ onSelectFile(attachType) }>
                  <FileUploadIcon/>
                </SelectFiles>
            }
          </ListItem>
          {
            selectedFiles?.[attachType] &&
              <ListItem>
                <ListItemText primary={ selectedFiles[attachType]?.name } />
              </ListItem>
          }
        </React.Fragment>
        )
      }
    </List>
    <Button fullWidth variant={ 'contained' } color='secondary' disabled={ loading } onClick={ onSaveBtn }>Save</Button>
  </>
}

export default SetMapToMesh
