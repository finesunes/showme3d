import { List, ListItemButton, ListItemText } from '@mui/material'
import React, { useState } from 'react'
import { AnimationDataType } from '../../../three/workspace/ModelBox3'

import PlayArrowIcon from '@mui/icons-material/PlayArrow'
import PauseIcon from '@mui/icons-material/Pause'
import { styled } from '@mui/system'

type Props = {
  animations: AnimationDataType[],
  uuid?: string
}

const StyledList = styled(List)(({ theme }) => ({
  marginBottom: theme.spacing(1.5),
  boxShadow: `0 0 6px ${theme.palette.primary.light}`,
  borderRadius: theme.spacing(0.5),
  '& > .MuiListItem-root': {
    cursor: 'pointer',
    justifyContent: 'space-between'
  }
}))

const MeshAnimationControl: React.FC<Props> = ({ animations }) => {
  if (animations.length < 1) {
    return <></>
  }

  const [activeAnimationUuid, setActiveAnimationUuid] = useState<string | null>(null)

  const onClickAnimation = (newClipUuid: string) => () => {
    if (activeAnimationUuid && newClipUuid === activeAnimationUuid) {
      animations.forEach(animation => {
        if (animation.clipUuid === newClipUuid) {
          animation.action.stop()
        }
      })

      setActiveAnimationUuid(null)
    } else if (newClipUuid !== activeAnimationUuid) {
      animations.forEach(animation => {
        if (animation.clipUuid === activeAnimationUuid) {
          animation.action.stop()
        }
        if (animation.clipUuid === newClipUuid) {
          animation.action.play()
        }
      })

      setActiveAnimationUuid(newClipUuid)
    }
  }

  return <StyledList>
    {
      animations.map(animation => {
        return <ListItemButton onClick={ onClickAnimation(animation.clipUuid) } key={ animation.clipUuid }>
          { activeAnimationUuid === animation.clipUuid ? <PauseIcon/> : <PlayArrowIcon/> }
          <ListItemText primary={ animation.name } />
        </ListItemButton>
      })
    }
  </StyledList>
}

export default React.memo(MeshAnimationControl)
