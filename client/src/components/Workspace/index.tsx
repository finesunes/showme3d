import React from 'react'

import Workspace from '../../three/workspace'
import GridContainer from './GridContainer'
import Controls from './Controls'
import { WorkspaceContext } from '../../context/WorkspaceContext'
import CubicLoader from '../Loader/CubicLoader'

import { toast } from 'react-toastify'
import { applyUrlSettingsToWorkspace } from '../../three/workspace/WorkspaceSettings'

import { debounce } from 'lodash'

import IdleScreen from '../IdleScreen'
import DragAndDrop from '../DragAndDrop'
import CanvasContainer from './CanvasContainer'
import { loadingLocalFiles } from '@utils/file'

type Props = {}

type State = {
  updateCounter: number
  controlsIsOpen: boolean
  loading: boolean
  drawer: boolean
}

class WorkspaceCanvas extends React.Component<Props, State> {
  divRef: HTMLDivElement | null = null
  workspaceThree: Workspace | null = null

  state = {
    updateCounter: 0,
    controlsIsOpen: false,
    loading: false,
    drawer: window.innerWidth > 600
  }

  updateWorkspace = () => this.setState({ updateCounter: this.state.updateCounter + 1 })
  setLoading = (value: boolean) => this.setState({ loading: value })

  loadingWorkspaceSettings = async () => {
    if (!this.workspaceThree) return

    this.setLoading(true)

    try {
      await applyUrlSettingsToWorkspace(this.workspaceThree)

      toast.success('Workspace is loaded', { toastId: 'workspace' })
    } catch (e) {
      toast.success('Workspace is not correctly loaded', { toastId: 'workspace' })
    } finally {
      this.setLoading(false)
    }
  }

  async componentDidMount () {
    if (!this.divRef) return

    if (!this.workspaceThree) {
      this.workspaceThree = new Workspace(this.divRef)
    }

    await this.loadingWorkspaceSettings()

    this.workspaceThree.firstRender()

    this.updateWorkspace()

    window.addEventListener('resize', this.debouncedResizeWorkspace)
  }

  resizeWorkspace = (): void => {
    if ((this.divRef?.clientWidth || this.divRef?.clientHeight) && this.workspaceThree) {
      this.workspaceThree.setCanvasSize(10, 10)
      this.workspaceThree.setCanvasSize(this.divRef?.clientWidth, this.divRef?.clientHeight)
    }
  }

  debouncedResizeWorkspace = debounce(this.resizeWorkspace, 300)

  setDrawer = (drawer: boolean): void => {
    this.setState({ drawer }, this.resizeWorkspace)
  }

  componentWillUnmount () {
    this.workspaceThree?.stopAnimation()

    window.removeEventListener('resize', this.debouncedResizeWorkspace)
  }

  onDropFiles = async (files: File[]) => {
    if (this.workspaceThree) {
      await loadingLocalFiles(files, this.workspaceThree, this.setLoading)
    }
  }

  render () {
    const { drawer, loading } = this.state

    return <>
      <GridContainer drawer={ drawer }>
        <WorkspaceContext.Provider value={
          {
            loading,
            drawer,
            workspace: this.workspaceThree,
            update: this.updateWorkspace,
            setLoading: this.setLoading,
            setDrawer: this.setDrawer
          }
        }>
          <DragAndDrop onDropFiles={ this.onDropFiles }>
            <CanvasContainer ref={ (divRef: HTMLDivElement) => { this.divRef = divRef } }>
              <CubicLoader loading={ loading } />
              <IdleScreen />
            </CanvasContainer>
          </DragAndDrop>
          <Controls />
        </WorkspaceContext.Provider>
      </GridContainer>
    </>
  }
}

export default WorkspaceCanvas
