import { styled } from '@mui/system'

type Props = {
  drawer?: boolean
}

const GridContainer = styled('div', { shouldForwardProp: propName => propName !== 'drawer' })<Props>(({ theme, drawer }) => ({
  display: 'grid',
  grid:
    `[row1-start] ${drawer ? '"header header"' : '"workspace workspace"'} 48px [row1-end]` +
    `[row2-start] "workspace ${drawer ? 'drawer' : 'workspace'}" 1fr[row2-end]` +
    '/ 9fr 3fr',
  height: '100%',
  position: 'relative',
  overflow: 'hidden',
  [theme.breakpoints.down('md')]: {
    grid:
      `[row1-start] ${drawer ? '"header header"' : '"workspace workspace"'} 48px [row1-end]` +
      '[row2-start] "workspace drawer" 1fr[row2-end]' +
      '/ 12fr 1px'
  }
}))

export default GridContainer
