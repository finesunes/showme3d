import React from 'react'
import { Link as RouterLink } from 'react-router-dom'

import { Link as MuiLink, LinkProps } from '@mui/material'

type Props = LinkProps & {
  to: string,
  children?: React.ReactNode,
}

const Link: React.FC<Props> = (props) => {
  return <MuiLink { ...props } component={ RouterLink }/>
}

export default Link
