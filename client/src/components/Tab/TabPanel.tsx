import React from 'react'
import { Box, BoxProps } from '@mui/material'

type TabPanelProps = BoxProps & {
  children?: React.ReactNode,
  index: number | string,
  value: number | string,
}

const TabPanel: React.FC<TabPanelProps> = ({ index, value, children, ...rest }) => {
  return <Box { ...rest } hidden={ index !== value }>
    { children }
  </Box>
}

export default TabPanel
