import React, { useState } from 'react'
import { Collapse, List, ListItemButton, ListItemText, ListProps } from '@mui/material'
import { ExpandLess, ExpandMore } from '@mui/icons-material'

type Props = ListProps & {
  buttonText: string
}

const CollapseList: React.FC<Props> = ({ children, buttonText, ...listProps }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false)

  return <List { ...listProps }>
    <ListItemButton onClick={ () => setIsOpen(!isOpen) }>
      <ListItemText primary={ buttonText } />
      { isOpen ? <ExpandLess /> : <ExpandMore /> }
    </ListItemButton>
    <Collapse in={ isOpen }>
      { children }
    </Collapse>
  </List>
}

export default React.memo(CollapseList)
