import React, { useState } from 'react'
import { ListItem, Slider, Typography, TextField } from '@mui/material'
import { styled } from '@mui/system'

const StyledListItem = styled(ListItem)`
  width: 100%;
  display: grid;
  grid: [row1-start] 'text text' [row1-end]
        [row2-start] 'slider input' [row2-end]
        / 1fr 64px;
  grid-row-gap: 4px;
  grid-column-gap: 16px;
`

type Props = {
  initialValue?: number,
  min?: number,
  max?: number,
  step?: number,
  text: string,
  icon?: React.ReactNode,
  onChangeFunc(...value: any): void,
}

const SliderListItem: React.FC<Props> = ({
  text,
  onChangeFunc,
  min = 0,
  max = 10,
  step = 0.1,
  initialValue = 0
}) => {
  const [value, setValue] = useState<number | string | Array<number | string>>(initialValue)

  const handleSliderChange = (event: Event, newValue: number | number[]) => {
    setValue(newValue)
    onChangeFunc(newValue)
  }

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value
    if (value === '') {
      setValue('')
    } else {
      setValue(Number(value))
      onChangeFunc(Number(value))
    }
  }

  const handleBlur = () => {
    if (value < min) {
      setValue(Number(min))
      onChangeFunc(Number(min))
    } else if (value > max) {
      setValue(Number(max))
      onChangeFunc(Number(max))
    }
  }

  return <StyledListItem>
    <Typography id="input-slider" sx={ { gridArea: 'text' } }>
      { text }
    </Typography>
    <Slider
      sx={ { gridArea: 'slider' } }
      value={ typeof value === 'number' ? value : min }
      onChange={ handleSliderChange }
      step={ step }
      min={ min }
      max={ max }
      aria-label={ `${text} slider` }
    />
    <TextField
      variant='outlined'
      sx={ { gridArea: 'input' } }
      value={ value }
      size="small"
      onChange={ handleInputChange }
      onBlur={ handleBlur }
      inputProps={ {
        step: step,
        min: min,
        max: max,
        type: 'number'
      } }
    />

  </StyledListItem>
}

export default React.memo(SliderListItem)
