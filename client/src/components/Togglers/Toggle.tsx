import React, { useState } from 'react'
import { ToggleButton, ToggleButtonProps } from '@mui/material'

type Props = ToggleButtonProps & {
  enableFunc(): any,
  disableFunc(): any,
  initialState: any,
  updateWorkspace?(): void,
}

const Toggle: React.FC<Props> = ({ enableFunc, disableFunc, initialState, updateWorkspace, ...rest }) => {
  const [isEnable, setIsEnable] = useState<boolean>(initialState)

  const onClick = () => {
    if (isEnable) {
      disableFunc()
    } else {
      enableFunc()
    }

    if (updateWorkspace) {
      updateWorkspace()
    }
    setIsEnable(!isEnable)
  }

  return <ToggleButton color='primary' { ...rest } selected={ isEnable } onClick={ onClick } />
}

export default React.memo(Toggle)
