import React from 'react'
import { ToggleButton, ToggleButtonGroup } from '@mui/material'

type ToggleDataType = {
  name: string
  enableFunc(): void
  children: React.ReactNode,
  ariaLabel?: string
}

type Props = {
  data: ToggleDataType[],
  active: string | null,
  disableFunc(): void
}

const ToggleGroup: React.FC<Props> = ({ data, active, disableFunc }) => {
  const [alignment, setAlignment] = React.useState<string | null>(active)

  const onChange = (event: React.MouseEvent<HTMLElement>, newAlignment: string | null) => {
    if (newAlignment) {
      data.forEach(toggleDta => {
        if (toggleDta.name === newAlignment) {
          toggleDta.enableFunc()
        }
      })
    } else {
      disableFunc()
    }

    setAlignment(newAlignment)
  }

  return <ToggleButtonGroup sx={ { flexWrap: 'wrap' } } onChange={ onChange } exclusive value={ alignment }>
    {
      data.map((toggleData, i) =>
        <ToggleButton aria-label={ toggleData.ariaLabel || 'Control button' } key={ i } value={ toggleData.name } size='small'>
         { toggleData.children }
        </ToggleButton>
      )
    }
  </ToggleButtonGroup>
}

export default React.memo(ToggleGroup)
