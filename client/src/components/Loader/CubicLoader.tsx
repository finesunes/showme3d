import React from 'react'

import Loader from './Loader'
import { Box, BoxProps } from '@mui/material'
import styled from '@mui/system/styled'

const Background = styled(Box)({
  position: 'absolute',
  width: '100%',
  height: '100%',
  background: 'white',
  opacity: '0.8'
})

type CubicWrapProps = {
  size: number
}

const CubicWrap = styled('div', { shouldForwardProp: propName => propName !== 'size' })<CubicWrapProps>(({ theme, size }) => {
  return {
    position: 'relative',
    display: 'inline-flex',
    flexWrap: 'wrap',
    width: `${size * 3}px`,
    height: `${size * 3}px`,
    transformStyle: 'preserve-3d',
    transform: 'rotateX(45deg) rotate(45deg)',

    '@keyframes move': {
      '0%, 100%': { transform: 'none' },
      '25%': { transform: `translate(${size * 2}px, 0)` },
      '50%': { transform: `translate(${size * 2}px, ${size * 2}px)` },
      '75%': { transform: `translate(0, ${size * 2}px)` }
    }
  }
})

type CubicProps = {
  size: number
}

const Cubic = styled('div', { shouldForwardProp: propName => propName !== 'size' })<CubicProps>(({ theme, size }) => {
  return {
    position: 'absolute',
    top: 0,
    left: 0,
    width: size,
    height: size,
    background: theme.palette.primary.light,
    animation: 'move 2s ease-in-out infinite both',
    transformStyle: 'preserve-3d',
    '&:nth-of-type(1)': {
      animationDelay: '-1s'
    },
    '&:nth-of-type(2)': {
      animationDelay: '-2s'
    },
    '&::before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      width: size,
      height: size,
      top: '100%',
      left: 0,
      background: theme.palette.primary.main,
      transformOrigin: 'center top',
      transform: 'rotateX(-90deg)'
    },
    '&::after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      width: size,
      height: size,
      top: 0,
      left: '100%',
      background: theme.palette.primary.dark,
      transformOrigin: 'center left',
      transform: 'rotateY(90deg)'
    }
  }
})

type Props = BoxProps & {
  loading?: boolean,
  size?: number
}

const CubicLoader: React.FC<Props> = ({ loading, size = 45 }) => {
  return <Loader loading={ loading }>
    <Background/>
    <CubicWrap size={ size }>
      <Cubic size={ size }/>
      <Cubic size={ size }/>
    </CubicWrap>
  </Loader>
}

export default CubicLoader
