import { Box, BoxProps } from '@mui/material'
import styled from '@mui/system/styled'

type LoaderProgressProps = BoxProps & {
  loading?: boolean
}

const Loader = styled(Box, { shouldForwardProp: propName => propName !== 'loading' })<LoaderProgressProps>(({ theme, loading }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  display: loading ? 'flex' : 'none',
  justifyContent: 'center',
  alignItems: 'center',
  zIndex: 2000
}))

export default Loader
