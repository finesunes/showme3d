import React, { useState } from 'react'

type Props = {
  children: React.ReactElement,
  onDropFiles(files: File[]): Promise<void>
}

const DragAndDrop: React.FC<Props> = ({ children, onDropFiles }) => {
  const [isDragging, setIsDragging] = useState<number>(0)

  const overrideEventDefaults = (e: Event | React.DragEvent<HTMLDivElement>): void => {
    e.preventDefault()
    e.stopPropagation()
  }

  const onDragEnter = (e: React.DragEvent<HTMLDivElement>): void => {
    overrideEventDefaults(e)
    setIsDragging(prev => prev + 1)
  }

  const onDragLeave = (e: React.DragEvent<HTMLDivElement>): void => {
    overrideEventDefaults(e)
    setIsDragging(prev => prev - 1)
  }

  const onDrop = async (e: React.DragEvent<HTMLDivElement>): Promise<void> => {
    onDragLeave(e)

    if (e?.dataTransfer?.files) {
      const files: File[] = Array.from(e.dataTransfer.files)

      await onDropFiles(files)
    }
  }

  return React.cloneElement(
    children,
    {
      onDragEnter: onDragEnter,
      onDragLeave: onDragLeave,
      onDragStart: overrideEventDefaults,
      onDragEnd: overrideEventDefaults,
      onDragOver: overrideEventDefaults,
      onDrop: onDrop,
      isDragging: !!isDragging
    }
  )
}

export default React.memo(DragAndDrop)
