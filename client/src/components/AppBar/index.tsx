import React from 'react'

import { AppBar as MuiAppBar, Box, IconButton, Toolbar } from '@mui/material'
import AddFiles from '../Button/AddFiles'

import AttachFileIcon from '@mui/icons-material/AttachFile'
import AddFilesByUrl from '../Button/AddFilesByUrl'

import MenuIcon from '@mui/icons-material/Menu'
import Share from '../Button/Share'
import { WorkspaceContext } from '../../context/WorkspaceContext'
import styled from '@mui/system/styled'
import ReloadAndClear from '../Button/ReloadAndClear'
import Link from '../Link'
import LogoImg from '../Button/LogoImg'

type MuiAppBarProps = {
  drawer?: boolean
}

const StyledMuiAppBar = styled(
  MuiAppBar,
  { shouldForwardProp: propName => propName !== 'drawer' }
)<MuiAppBarProps>(({ theme, drawer }) => ({
  gridArea: 'header',
  background: drawer ? theme.palette.primary.main : 'transparent',
  position: drawer ? 'relative' : 'fixed',
  zIndex: 3100,
  '& .MuiButtonBase-root': {
    color: drawer ? theme.palette.primary.contrastText : theme.palette.grey[500]
    // height: '40px'
  }
}))

type Props = {}

const AppBar: React.FC<Props> = () => {
  const { drawer, setDrawer } = React.useContext(WorkspaceContext)

  return <StyledMuiAppBar drawer={ drawer } elevation={ 0 }>
    <Toolbar sx={ { justifyContent: 'space-between', minHeight: '48px' } } variant='dense'>
      <Box sx={ { display: 'flex', justifyContent: 'center' } }>
        <Link target='_blank' to='/about' sx={ { width: '40px', height: '40px' } }>
          <LogoImg src='/logo.svg' alt='logo' />
        </Link>
        <AddFiles component={ IconButton } aria-label='Add local files'><AttachFileIcon/></AddFiles>
        <AddFilesByUrl component={ IconButton } aria-label='Add files by url' />
        <Share />
        <ReloadAndClear/>
      </Box>
      <div>
        <IconButton aria-label='Menu' onClick={ () => setDrawer(!drawer) }>
          <MenuIcon />
        </IconButton>
      </div>
    </Toolbar>
  </StyledMuiAppBar>
}

export default React.memo(AppBar)
