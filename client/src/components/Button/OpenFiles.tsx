import React from 'react'
import { useHistory } from 'react-router'

import { Button, ButtonProps } from '@mui/material'

type Props = ButtonProps & {
  files: File[],
  path?: string
}

const OpenFilesButton: React.FC<Props> = ({ files = [], path = '/workspace', ...rest }) => {
  const history = useHistory()

  const onClickHandler = () => {
    history.push(path, files)
  }

  return <Button { ...rest } onClick={ onClickHandler } />
}

export default OpenFilesButton
