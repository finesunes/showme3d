import React from 'react'

import { ButtonProps } from '@mui/material'
import SelectFiles from './SelectFiles'
import { loadingLocalFiles } from '@utils/file'
import { WorkspaceContext } from '../../context/WorkspaceContext'

type Props = ButtonProps & {
  component?: React.ElementType
}

const AddFiles: React.FC<Props> = ({ ...rest }) => {
  const { workspace, loading, setLoading } = React.useContext(WorkspaceContext)
  if (!workspace) {
    return <></>
  }

  const onSelectFiles = async (files: File[]): Promise<void> => {
    if (files) {
      await loadingLocalFiles(files, workspace, setLoading)
    }
  }

  return <SelectFiles onSelectFiles={ onSelectFiles } { ...rest } disabled={ loading } />
}

export default React.memo(AddFiles)
