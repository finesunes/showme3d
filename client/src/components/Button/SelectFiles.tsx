import React from 'react'
import { Box, Button, ButtonProps } from '@mui/material'
import { styled } from '@mui/system'

const HiddenInput = styled('input')({
  display: 'none'
})

type OptionsType = {
  multiple?: boolean
}

type BtnProps = ButtonProps & {
  onSelectFiles(files: File[]): void,
  options: OptionsType,
  component?: React.ElementType
}

type CommonProps = ButtonProps & {
  onSelectFiles(files: File[]): void,
  options?: OptionsType,
  component?: React.ElementType
}

const SelectFilesBtnWithInput: React.FC<BtnProps> = ({ onSelectFiles, options, component, ...rest }) => {
  const onChangeFiles = (e: React.FormEvent<HTMLInputElement>): void => {
    if (!e.currentTarget.files) return

    const files: File[] = Array.from(e.currentTarget.files)
    if (files) {
      onSelectFiles(files)
    }

    e.currentTarget.value = ''
  }

  return <Box component={ 'label' }>
    {
      component
        ? React.createElement(
          component,
          {
            ...rest,
            component: 'span'
          }
        )
        : <Button { ...rest } component={ 'span' } />
    }
    <HiddenInput type={ 'file' } multiple={ options.multiple } onChange={ onChangeFiles }/>
  </Box>
}

const SelectFilesBtnWithPicker: React.FC<BtnProps> = ({ onSelectFiles, options, ...rest }) => {
  const onBtnClick = async (): Promise<void> => {
    try {
      // @ts-ignore
      const fileHandlers = await window.showOpenFilePicker(options)
      const files: File[] = []

      ;(await Promise.allSettled((fileHandlers as Array<any>).map(fileHandler => fileHandler.getFile())))
        .forEach(res => {
          if (res.status === 'fulfilled') {
            files.push(res.value)
          }
        })

      if (files) {
        onSelectFiles(files)
      }
    } catch (e) {
      console.log(e)
    }
  }

  return <Box component={ Button } { ...rest } onClick={ onBtnClick } />
}

const SelectFilesBtn: React.FC<CommonProps> = ({
  onSelectFiles,
  options = { multiple: true }
  , ...rest
}) => {
  // @ts-ignore
  if (window.showOpenFilePicker) {
    return <SelectFilesBtnWithPicker
      { ...rest }
      options={ options }
      onSelectFiles={ onSelectFiles }
    />
  } else {
    return <SelectFilesBtnWithInput
      { ...rest }
      options={ options }
      onSelectFiles={ onSelectFiles }
    />
  }
}

export default SelectFilesBtn
