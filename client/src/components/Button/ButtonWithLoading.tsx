import React from 'react'
import { Button, ButtonProps, CircularProgress } from '@mui/material'

type ButtonWithLoadingProps = ButtonProps & {
  loading?: boolean;
}

const ButtonWithLoading: React.FC<ButtonWithLoadingProps> = ({ children, loading, ...rest }) => {
  return <Button { ...rest } disabled={ loading }>
    {
      loading
        ? <CircularProgress color={ rest.color } />
        : { children }
    }
  </Button>
}

export default ButtonWithLoading
