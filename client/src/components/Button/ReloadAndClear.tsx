import React, { useState } from 'react'
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton } from '@mui/material'
import CachedIcon from '@mui/icons-material/Cached'

type Props = {}

const ReloadAndClear: React.FC<Props> = () => {
  const [open, setOpen] = useState<boolean>(false)

  const onReloadBtnClick = () => {
    window.location.assign('/')
  }

  const onOpenBtnClick = () => setOpen(true)
  const onClose = () => setOpen(false)

  return <>
    <IconButton onClick={ onOpenBtnClick }><CachedIcon/></IconButton>
    <Dialog
      sx={ { zIndex: 2000 } }
      open={ open }
      onClose={ onClose }
      aria-labelledby="reload-dialog-title"
      aria-describedby="reload-dialog-description"
    >
      <DialogTitle id="reload-dialog-title">
        { 'Reload and clear workspace?' }
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="reload-dialog-description">
          All loaded models will be deleted and all settings will be reset.
          Are you sure you want to reload and clear workspace?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={ onClose } autoFocus>Close dialog</Button>
        <Button variant='contained' color='warning' onClick={ onReloadBtnClick }>
          Reload and clear workspace
        </Button>
      </DialogActions>
    </Dialog>
  </>
}

export default ReloadAndClear
