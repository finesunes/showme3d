import styled from '@mui/system/styled'

const LogoImg = styled('img')(() => ({
  height: '100%',
  maxHeight: '40px',
  cursor: 'pointer',
  outlineWidth: '0px !important',
  userSelect: 'none'
}))

export default LogoImg
