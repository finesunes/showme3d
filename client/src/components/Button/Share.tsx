import React, { useState } from 'react'
import { Box, Button, DialogActions, DialogContent, IconButton, Typography, OutlinedInput } from '@mui/material'
import IosShareIcon from '@mui/icons-material/IosShare'
import { WorkspaceContext } from '../../context/WorkspaceContext'
import { createShareUrl } from '../../three/workspace/WorkspaceSettings'
import { toast } from 'react-toastify'
import Dialog from '../Dialog'

const Share: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false)
  const [shareUrl, setShareUrl] = useState<string>('')

  const { workspace } = React.useContext(WorkspaceContext)
  if (!workspace) {
    return <></>
  }

  const onCopyBtn = async () => {
    await toast.promise(navigator.clipboard.writeText(shareUrl), {
      pending: 'Wait!',
      success: 'The share URL is saved to the clipboard',
      error: 'URL is not saved to the clipboard'
    }, { toastId: 'workspace' })
  }

  const onOpenBtn = () => {
    setOpen(true)
    const url = createShareUrl(workspace)

    setShareUrl(url)
  }
  const onClose = () => setOpen(false)

  return <>
    <IconButton aria-label='Share' onClick={ onOpenBtn }><IosShareIcon/></IconButton>
    <Dialog onClose={ onClose } open={ open } title='Share'>
      <DialogContent>
        <Box>
          <Typography>Sharing Link</Typography>
            <OutlinedInput
              disabled
              fullWidth
              defaultValue={ shareUrl }
              endAdornment={ <Button onClick={ onCopyBtn } variant='outlined'>Copy</Button> }
            />
        </Box>

      </DialogContent>
      <DialogActions>
        <Button variant='contained' onClick={ onClose }>Close</Button>
      </DialogActions>
    </Dialog>
  </>
}

export default React.memo(Share)
