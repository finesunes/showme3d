import React, { useState } from 'react'
import Dialog from '../Dialog'
import { Box, Button, ButtonProps, DialogActions, DialogContent, TextField } from '@mui/material'
import { WorkspaceContext } from '../../context/WorkspaceContext'
import LinkIcon from '@mui/icons-material/Link'
import { loadingByUrlFiles } from '@utils/file'

const textFiledModelsId = 'upload-files-by-url'
const textFiledArchivesId = 'upload-archives-by-url'

type Props = ButtonProps & {
  component?: React.ElementType
}

const AddFilesByUrl: React.FC<Props> = ({ ...rest }) => {
  const [open, setOpen] = useState<boolean>(false)

  const { workspace, loading, setLoading } = React.useContext(WorkspaceContext)
  if (!workspace) {
    return <></>
  }

  const { models, archives } = workspace.models.getModelsShareData()

  const loadedModelsUrls = models.map(model => model.src)
  const defaultValueModelUrls = loadedModelsUrls.join(' ')

  const loadedArchivesUrls = archives.map(archive => archive.url)
  const defaultValueArchivesUrls = loadedArchivesUrls.join(' ')

  const onUploadBtn = async () => {
    onClose()

    const textFieldModels: HTMLInputElement = document.getElementById(textFiledModelsId) as HTMLInputElement
    const textFieldArchives: HTMLInputElement = document.getElementById(textFiledArchivesId) as HTMLInputElement

    const modelsUrls = textFieldModels.value.split(/[ \t\n]+/)
      .filter(Boolean)
      .filter(v => loadedModelsUrls.indexOf(v) === -1)
    const archivesUrls = textFieldArchives.value
      .split(/[ \t\n]+/)
      .filter(Boolean)
      .filter(v => loadedArchivesUrls.indexOf(v) === -1)

    await loadingByUrlFiles(modelsUrls, archivesUrls, workspace, setLoading)
  }

  const onOpenBtn = () => setOpen(true)
  const onClose = () => setOpen(false)

  return <>
    <Box component={ Button } { ...rest } disabled={ loading } onClick={ onOpenBtn }><LinkIcon/></Box>
    <Dialog onClose={ onClose } open={ open } title='Upload files by url'>
      <DialogContent>
          <TextField sx={ { mt: 1, mb: 4 } }
            defaultValue={ defaultValueModelUrls }
            id={ textFiledModelsId } label={ 'Input here your urls' } fullWidth multiline variant={ 'outlined' } minRows={ 4 } />

          <TextField
            defaultValue={ defaultValueArchivesUrls }
            id={ textFiledArchivesId } label={ 'Input here your archives urls' } fullWidth multiline variant={ 'outlined' } />
      </DialogContent>
      <DialogActions>
        <Button disabled={ loading } onClick={ onClose }>Close</Button>
        <Button variant={ 'contained' } color='secondary' disabled={ loading } onClick={ onUploadBtn }>Upload and open</Button>
      </DialogActions>
    </Dialog>
  </>
}

export default React.memo(AddFilesByUrl)
