import React from 'react'

import { styled } from '@mui/system'
import { Dialog, DialogTitle, DialogTitleProps, IconButton } from '@mui/material'
import CloseIcon from '@mui/icons-material/Close'

const StyledDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
    minWidth: '500px'
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(2)
  },
  [theme.breakpoints.down('md')]: {
    '& .MuiDialogContent-root': {
      minWidth: '50vw'
    }
  }
}))

type StyledDialogTitleProps = DialogTitleProps & {
  onClose(): void,
  children: React.ReactNode
}

const StyledDialogTitle: React.FC<StyledDialogTitleProps> = (props) => {
  const { children, onClose, ...other } = props

  return (
    <DialogTitle sx={ { m: 0, p: 2, pr: 6 } } { ...other }>
      { children }
      { onClose
        ? (
        <IconButton
          aria-label="close"
          onClick={ onClose }
          sx={ {
            position: 'absolute',
            right: theme => theme.spacing(1),
            top: theme => theme.spacing(1),
            color: theme => theme.palette.grey[500]
          } }
        >
          <CloseIcon />
        </IconButton>
          )
        : null }
    </DialogTitle>
  )
}

type MainProps = {
  open?: boolean,
  onClose(): void,
  children: NonNullable<React.ReactNode>,
  title?: string,
  scroll?: 'body' | 'paper'
}

const PrimaryDialog: React.FC<MainProps> = ({
  open = false,
  onClose,
  children,
  title,
  scroll
}) => {
  return <StyledDialog scroll={ scroll } open={ open } onClose={ onClose }>
    { title && <StyledDialogTitle onClose={ onClose }>{ title }</StyledDialogTitle> }
    { children }
  </StyledDialog>
}

export default PrimaryDialog
