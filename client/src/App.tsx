import React, { Component } from 'react'

import AppRouter from './AppRouter'

import { ThemeProvider } from '@mui/material/styles'
import { CssBaseline } from '@mui/material'
import createMyTheme from './themes/Light'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const theme = createMyTheme()

class App extends Component {
  render () {
    return <ThemeProvider theme={ theme }>
      <CssBaseline />
        <AppRouter />
      <ToastContainer
        position="bottom-left"
        limit={ 3 }
      />
    </ThemeProvider>
  }
}

export default App
