import React, { createContext } from 'react'
import Workspace from '../three/workspace'

export type WorkspaceContextType = {
  workspace: Workspace | null
  update:() => void
  loading: boolean
  setLoading: (value: boolean) => void
  drawer: boolean
  setDrawer: (value: boolean) => void
}

export const WorkspaceContext =
  createContext<WorkspaceContextType>({
    workspace: null,
    update: () => {},
    loading: false,
    setLoading: () => {},
    drawer: false,
    setDrawer: () => {}
  })

type Props = WorkspaceContextType

type Omit<T, K> = Pick<T, Exclude<keyof T, K>>;

export const withWorkspace = <P extends object>(
  Component: React.ComponentType<P>
): React.FC<Omit<P, keyof Props>> => props => (
  <WorkspaceContext.Consumer>
    { workspaceContext => <Component { ...props as P } workspaceContext={ workspaceContext } /> }
  </WorkspaceContext.Consumer>
  )
