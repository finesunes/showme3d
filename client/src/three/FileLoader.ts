import * as THREE from 'three'

import { getExt, imgAvailableExt, modelAvailableExt, modelExtension } from '@utils/file'

import URI from 'urijs'

import { ModelSettings } from '@utils'

export type LoaderResponseType = {
  value: THREE.Object3D,
  url?: string,
  settings?: ModelSettings,
  archiveUrl?: string,
  name?: string,
}

export type LoadingData = {
  src: (File | string),
  settings: ModelSettings,
  archiveUrl?: string
}

class FileLoader {
  private currentProgress: number
  private totalProgress: number

  textures: Array<File | string> = []
  mtls: Array<File | string> = []

  constructor () {
    this.currentProgress = 0
    this.totalProgress = 0
  }

  isLoadingData = (value: any): boolean => !!(value.src && value.settings)

  load = async (
    sources: Array<File | string | LoadingData>,
    loadCallback: (object: LoaderResponseType) => void
  ): Promise<void> => {
    try {
      if (!sources?.length) {
        return
      }

      const modelsData: Array<File | string | LoadingData> = []

      sources.forEach(src => {
        let ext: string = ''
        if (typeof src === 'object' && this.isLoadingData(src)) {
          const _src = (src as LoadingData).src
          ext = getExt(_src instanceof File ? _src.name : _src)
        }
        if (typeof src === 'string' || src instanceof File) {
          ext = getExt(src instanceof File ? src.name : src)
        }

        switch (true) {
          case ext === 'mtl':
            if (typeof src === 'object' && this.isLoadingData(src)) {
              this.mtls.push((src as LoadingData).src)
            } else {
              this.mtls.push(src as (File | string))
            }
            break
          case imgAvailableExt.indexOf(ext) > -1:
            if (typeof src === 'object' && this.isLoadingData(src)) {
              this.textures.push((src as LoadingData).src)
            } else {
              this.textures.push(src as (File | string))
            }
            break
          case modelAvailableExt.indexOf(ext) > -1:
            modelsData.push(src)
            break
          default:
            break
        }
      })

      ;(await Promise.allSettled(
        modelsData.map(async src => {
          if (typeof src === 'object' && this.isLoadingData(src)) {
            const _src = src as LoadingData
            const res = await this.callLoaderByFileExt(_src.src)

            if (loadCallback && res.value) {
              res.settings = _src.settings
              res.archiveUrl = _src.archiveUrl

              const { name } = this.prepareSrc(_src.src)
              if (name) {
                res.name = name
              }

              if (typeof _src.src === 'string') {
                res.url = _src.src
              }

              loadCallback(res)
            }

            return res
          } else {
            const _src = src as (File | string)
            const res = await this.callLoaderByFileExt(_src)

            if (loadCallback && res.value) {
              const { name } = this.prepareSrc(_src)
              if (name) {
                res.name = name
              }

              if (typeof src === 'string') {
                res.url = src
              }

              loadCallback(res)
            }

            return res
          }
        }))
      )

      this.textures.length = 0
      this.mtls.length = 0
    } catch (e) {
      console.log(e)
    }
  }

  callLoaderByFileExt = (src: File | string): Promise<LoaderResponseType> => {
    const ext = src instanceof File ? getExt(src.name) : getExt(src)

    if (!ext) {
      return new Promise((resolve, reject) => reject(new Error(`Invalid file extension ${name}`)))
    }

    switch (ext) {
      case modelExtension.fbx:
        return this.loadFBX(src)
      case modelExtension.obj:
        return this.loadOBJ(src)
      case modelExtension.glb:
      case modelExtension.gltf:
        return this.loadGLTF(src)
      case modelExtension.ply:
        return this.loadPLY(src)
      case modelExtension.stl:
        return this.loadSTL(src)
      case modelExtension.rhino3dm:
        return this.load3DM(src)
      case modelExtension.tds3ds:
        return this.load3DS(src)
      case modelExtension.threeMF:
        return this.load3MF(src)
      case modelExtension.amf:
        return this.loadAMF(src)
      case modelExtension.pcd:
        return this.loadPCD(src)
      case modelExtension.pmx:
      case modelExtension.pmd:
        return this.loadMMD(src)
      case modelExtension.prwm:
        return this.loadPRWM(src)
      case modelExtension.gcode:
        return this.loadGCode(src)
      case modelExtension.ifc:
        return this.loadIFC(src)
      case modelExtension.vox:
        return this.loadVOX(src)
      case modelExtension.titl:
        return this.loadTILT(src)
      default:
        return new Promise((resolve, reject) => reject(new Error('Not supported file extension')))
    }
  }

  getNameFromUrl = (url: string) => {
    try {
      return new URI(url).filename(true)
    } catch (e) {
      return 'unnamed'
    }
  }

  prepareSrc = (value: string | File): { url: string, name: string } => {
    return {
      url: value instanceof File ? URL.createObjectURL(value) : value,
      name: value instanceof File ? value.name : this.getNameFromUrl(value)
    }
  }

  // MATERIALS AND TEXTURES

  // return MTLLoader.MaterialCreator
  loadMTL = async (src: File | string): Promise<any> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/MTLLoader').then(({ MTLLoader }) => {
      const loader = new MTLLoader()

      loader.setMaterialOptions({ side: THREE.DoubleSide })

      const { url: mtlUrl } = this.prepareSrc(src)

      loader.load(mtlUrl, mtl => {
        for (const materialsInfoKey in mtl.materialsInfo) {
          this.textures.forEach(textureFile => {
            const { url: textureUrl, name } = this.prepareSrc(textureFile)

            if (mtl.materialsInfo[materialsInfoKey].map_kd === name) {
              mtl.materialsInfo[materialsInfoKey].map_kd = new URI(new URI(textureUrl).pathname()).segment(0)
            }
          })
        }

        resolve(mtl)
      }, () => {}, error => reject(error))
    })
  })

  loadTexture = async (src: File | string): Promise<THREE.Texture> => new Promise((resolve, reject) => {
    const loader = new THREE.TextureLoader()

    const { url } = this.prepareSrc(src)

    loader.load(url, (texture) => {
      resolve(texture)
    }, () => {
    }, () => {
      reject(new Error('load TEXTURE error'))
    })
  });

  loadCubeTexture = async (sources: (File | string)[]): Promise<THREE.CubeTexture> => new Promise((resolve, reject) => {
    const loader = new THREE.CubeTextureLoader()

    const urls: string[] = sources.map((v) => this.prepareSrc(v).url)

    loader.load(urls, (texture) => {
      resolve(texture)
    }, () => {
    }, () => {
      reject(new Error('load CUBE TEXTURE error'))
    })
  });

  // MODELS

  loadOBJ = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve) => {
    import('three/examples/jsm/loaders/OBJLoader').then(({ OBJLoader }) => {
      const loader = new OBJLoader()

      const { url, name } = this.prepareSrc(src)

      loader.load(url, obj => {
        obj.userData = { name }

        const res = { value: obj }

        // @ts-ignore
        const mtlNames: string[] = obj.materialLibraries
        if (mtlNames && mtlNames[0]) {
          const mtlFile = this.mtls.find(mtlFile => this.prepareSrc(mtlFile).name === mtlNames[0])
          if (mtlFile) {
            this.loadMTL(mtlFile).then(materialCreator => {
              loader.setMaterials(materialCreator)
              loader.load(url, objWithMtl => {
                objWithMtl.userData = { name }

                resolve({ value: objWithMtl })
              })
            })
          } else {
            resolve(res)
          }
        } else {
          resolve(res)
        }
      })
    })
  })

  loadGLTF = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/GLTFLoader').then(({ GLTFLoader }) => {
      import('three/examples/jsm/loaders/DRACOLoader').then(({ DRACOLoader }) => {
        const loader = new GLTFLoader()
        const { url, name } = this.prepareSrc(src)

        const dracoLoader = new DRACOLoader()
        dracoLoader.setDecoderPath('https://raw.githubusercontent.com/mrdoob/three.js/master/examples/js/libs/draco/')

        loader.setDRACOLoader(dracoLoader)

        loader.load(url, gltf => {
          gltf.scene.animations = gltf.animations.concat(gltf.scene.animations)

          gltf.scene.userData = { name }

          resolve({ value: gltf.scene })
        }, () => {
        }, error => reject(error))
      })
    })
  });

  loadPLY = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/PLYLoader').then(({ PLYLoader }) => {
      const loader = new PLYLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.computeVertexNormals()
        const _model = new THREE.Mesh(geometry, new THREE.MeshPhysicalMaterial())

        _model.userData = { name }

        resolve({ value: _model })
      }, () => {
      }, error => reject(error))
    })
  });

  loadSTL = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/STLLoader').then(({ STLLoader }) => {
      const loader = new STLLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        const _model = new THREE.Mesh(geometry, new THREE.MeshPhysicalMaterial())

        _model.userData = { name }

        resolve({ value: _model })
      }, () => {
      }, error => reject(error))
    })
  });

  loadFBX = (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/FBXLoader').then(({ FBXLoader }) => {
      const loader = new FBXLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, fbx => {
        fbx.traverse((child) => {
          if ((child as THREE.SkinnedMesh).isSkinnedMesh) {
            const skinnedMesh = child as THREE.SkinnedMesh

            skinnedMesh.bind(skinnedMesh.skeleton)
          }
        })

        fbx.userData = { name }

        resolve({ value: fbx })
      }, () => {}, error => reject(error))
    })
  })

  load3DM = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/3DMLoader').then(({ Rhino3dmLoader }) => {
      const loader = new Rhino3dmLoader()
      const { url, name } = this.prepareSrc(src)

      loader.setLibraryPath('https://cdn.jsdelivr.net/npm/rhino3dm@0.15.0-beta/')
      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  load3DS = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/TDSLoader').then(({ TDSLoader }) => {
      const loader = new TDSLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  load3MF = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/3MFLoader').then(({ ThreeMFLoader }) => {
      const loader = new ThreeMFLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  loadAMF = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/AMFLoader').then(({ AMFLoader }) => {
      const loader = new AMFLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  loadPCD = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/PCDLoader').then(({ PCDLoader }) => {
      const loader = new PCDLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        geometry.geometry.center()

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  loadMMD = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/MMDLoader').then(({ MMDLoader }) => {
      const loader = new MMDLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  loadPRWM = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/PRWMLoader').then(({ PRWMLoader }) => {
      const loader = new PRWMLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        const _model = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial())

        _model.userData = { name }

        resolve({ value: _model })
      }, () => {
      }, error => reject(error))
    })
  });

  loadGCode = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/GCodeLoader').then(({ GCodeLoader }) => {
      const loader = new GCodeLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  loadIFC = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/IFCLoader').then(({ IFCLoader }) => {
      const loader = new IFCLoader()
      const { url, name } = this.prepareSrc(src)

      loader.ifcManager.setWasmPath('ifc/')
      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });

  loadVOX = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/VOXLoader').then(({ VOXLoader, VOXMesh }) => {
      const loader = new VOXLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, chunks => {
        const group = new THREE.Group()

        chunks.forEach((chunk, i) => {
          const mesh = new VOXMesh(chunk)
          mesh.userData = { name: `chunk_${i}` }

          group.add(mesh)
        })

        group.userData = { name }

        resolve({ value: group })
      }, () => {
      }, error => reject(error))
    })
  });

  loadTILT = async (src: File | string): Promise<LoaderResponseType> => new Promise((resolve, reject) => {
    import('three/examples/jsm/loaders/TiltLoader').then(({ TiltLoader }) => {
      const loader = new TiltLoader()
      const { url, name } = this.prepareSrc(src)

      loader.load(url, geometry => {
        geometry.userData = { name }

        resolve({ value: geometry })
      }, () => {
      }, error => reject(error))
    })
  });
}

export default FileLoader
