import * as THREE from 'three'

import SceneFacade from './SceneFacade'
import CameraFacade from './CameraFacade'
import Models from './Models'
import FileLoader, { LoadingData } from '../FileLoader'

class Workspace {
  renderer: THREE.WebGL1Renderer | THREE.WebGLRenderer;

  scene: SceneFacade;

  camera: CameraFacade;

  models: Models

  allMixers: THREE.AnimationMixer[] = []

  clock: THREE.Clock = new THREE.Clock()

  _autoAnimate: boolean = true

  w: number = 200 // width
  h: number = 100 // height

  element: HTMLDivElement | null = null

  requestAnimationID = 0

  constructor (element: HTMLDivElement) {
    this.element = element
    this.w = element.clientWidth
    this.h = element.clientHeight

    this.renderer = new THREE.WebGLRenderer()
    this.renderer.setSize(this.w, this.h)

    this.renderer.setPixelRatio(window.devicePixelRatio)

    this.element.appendChild(this.renderer.domElement)

    this.scene = new SceneFacade()
      .enableGridHelper()

    this.scene.background = new THREE.Color()

    this.models = new Models(this.scene)

    this.camera = new CameraFacade(this.scene, this.renderer.domElement, 75, this.w / this.h)
      .createOrbitControls()
      .createDragControls(this.models)
      .createTransformControls(this.models)

    this.camera.setRenderFunc(this.render)
    this.scene.setRenderFunc(this.render)
    this.models.setRenderFunc(this.render)

    this.camera.position.set(0, 5, 5)

    this.camera.updateProjectionMatrix()
  }

  setCanvasSize = (width: number, height: number) => {
    this.w = width
    this.h = height

    this.renderer.setSize(this.w, this.h)
    this.renderer.setPixelRatio(window.devicePixelRatio)

    this.camera.aspect = this.w / this.h
    this.camera.updateProjectionMatrix()

    this.render()
  }

  get autoAnimate (): boolean {
    return this._autoAnimate
  }

  set autoAnimate (value: boolean) {
    this._autoAnimate = value

    if (!value) {
      this.stopAnimation()
    } else {
      this.animate()
    }

    this.camera.setAutoAnimate(value)
    this.scene.setAutoAnimate(value)
    this.models.setAutoAnimate(value)
  }

  animate = () => {
    if (!this.autoAnimate) {
      this.stopAnimation()
      return
    }

    this.requestAnimationID = requestAnimationFrame(this.animate)

    if (this.camera.orbitControls) {
      this.camera.orbitControls.update()
    }

    if (this.scene.cameraLight) {
      this.scene.cameraLight.position.copy(this.camera.position)
    }

    if (this.models?.length) {
      const clockDelta = this.clock.getDelta()
      this.allMixers.forEach(mixer => mixer.update(clockDelta))
    }

    this.render()

    return this.stopAnimation
  }

  stopAnimation = () => {
    if (this.requestAnimationID) {
      cancelAnimationFrame(this.requestAnimationID)
    }
  }

  loadFiles = async (
    sources: Array<File | string | LoadingData>
  ): Promise<boolean> => {
    this.stopAnimation()
    try {
      if (!sources || !sources.length) {
        return false
      }
      const fileLoader = new FileLoader()

      await fileLoader.load(sources, (res) => this.models.addOne(res))

      this.allMixers = this.models.allMixers

      this.render()

      await new Promise(resolve => {
        setTimeout(() => {
          this.render()

          resolve(undefined)
        }, 300)
      })

      return true
    } catch (e) {
      console.log(e)
      return false
    } finally {
      this.animate()
    }
  }

  firstRender = () => {
    this.camera.orbitControls?.update()

    if (this.scene.cameraLight) {
      this.scene.cameraLight.position.copy(this.camera.position)
    }

    this.render()
  }

  render = () => {
    this.renderer.render(this.scene, this.camera)
  };
}

export default Workspace
