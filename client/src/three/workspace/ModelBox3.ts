import * as THREE from 'three'
import { Vector3 } from 'three'
import { AttachMapEnum, ModelSettings } from '@utils'

const minInfinityV3 = new Vector3(Infinity, Infinity, Infinity)
const maxInfinityV3 = new Vector3(-Infinity, -Infinity, -Infinity)

const invisibleMaterial = new THREE.MeshBasicMaterial({ wireframe: true, visible: false, color: 0x00FF00 })
const wireframeMaterial = new THREE.MeshBasicMaterial({ wireframe: true, visible: true, color: 0x00FF00 })

export type AnimationDataType = {
  action: THREE.AnimationAction,
  name: string,
  clipUuid: string
}

export type AnimationsDataType = {
  uuid: string,
  animations: AnimationDataType[],
}

class ModelBox3 extends THREE.Mesh {
  modelCenter: THREE.Vector3 = new Vector3()
  boxSize: THREE.Vector3 = new THREE.Vector3()
  bindingModel: THREE.Object3D

  private _animationData: AnimationsDataType[] = []
  private _mixers: THREE.AnimationMixer[] = []
  skeleton: THREE.SkeletonHelper

  fileName: string = ''

  private _visibleBox: boolean = false

  constructor (object: THREE.Object3D) {
    super()

    this.bindingModel = object

    if (this.bindingModel.userData?.name) {
      this.fileName = this.bindingModel.userData?.name
    }

    this.skeleton = new THREE.SkeletonHelper(this.bindingModel)
    this.skeleton.visible = false
  }

  init (): ModelBox3 {
    this.createModelBox()

    return this
  }

  private createModelBox = (): void => {
    const minV3 = new Vector3().copy(minInfinityV3)
    const maxV3 = new Vector3().copy(maxInfinityV3)

    this.bindingModel.traverse(child => {
      if ((child as THREE.Mesh).isMesh) {
        const mesh = child as THREE.Mesh

        // mesh.updateMatrix()
        // mesh.frustumCulled = false
        // mesh.geometry.computeVertexNormals()

        // if (mesh.material && Array.isArray(mesh.material)) {
        //   mesh.material.forEach(v => {
        //     v.side = THREE.DoubleSide
        //     v.transparent = false
        //     v.needsUpdate = true
        //   })
        // } else {
        //   mesh.material.side = THREE.DoubleSide
        //   // mesh.material.transparent = false
        //   mesh.material.needsUpdate = true
        //   // console.log('mesh.material.flatShading', mesh.material.flatShading)
        //   // mesh.material.flatShading = true
        //   // mesh.material.needsUpdate = true
        // }

        if (mesh.geometry.boundingBox?.min) {
          minV3.min(mesh.geometry.boundingBox?.min)
        }
        if (mesh.geometry.boundingBox?.max) {
          maxV3.max(mesh.geometry.boundingBox?.max)
        }
      }

      if (child.animations && child.animations.length) {
        const mixer = new THREE.AnimationMixer(child)
        this._mixers.push(mixer)

        const animationData: AnimationsDataType = {
          uuid: child.uuid,
          animations: []
        }

        child.animations.forEach((animation, i) => {
          animationData.animations.push(
            { action: mixer.clipAction(animation), name: animation.name || `animation ${i}`, clipUuid: animation.uuid }
          )
        })

        this._animationData.push(animationData)
      }
    })

    const boundingBox = new THREE.Box3(minV3, maxV3)
    const autoBoundingBox = new THREE.Box3().setFromObject(this.bindingModel)

    const calcSize = boundingBox.getSize(new Vector3())
    const autoCalcSize = autoBoundingBox.getSize(new Vector3())

    this.boxSize = autoCalcSize.max(calcSize)

    this.geometry = new THREE.BoxGeometry(this.boxSize.x, this.boxSize.y, this.boxSize.z)
    this.material = invisibleMaterial

    if (!minV3.equals(minInfinityV3) && !maxV3.equals(maxInfinityV3)) {
      boundingBox.getCenter(this.modelCenter)
    } else {
      autoBoundingBox.getCenter(this.modelCenter)
    }

    this.geometry.translate(this.modelCenter.x, this.modelCenter.y, this.modelCenter.z)
    this.position.copy(this.bindingModel.position)
  }

  updateModel = () => {
    this.bindingModel.rotation.copy(this.rotation)

    this.bindingModel.scale.copy(this.scale)

    this.bindingModel.position.copy(this.position)
  };

  updateModelPosition = () => {
    this.bindingModel.position.copy(this.position)
  };

  get visibleBox (): boolean {
    return this._visibleBox
  }

  set visibleBox (value: boolean) {
    this._visibleBox = value

    if (value) {
      this.material = wireframeMaterial
    } else {
      this.material = invisibleMaterial
    }
  }

  get allChildrenData (): { meshes: THREE.Mesh[], animationsData: AnimationsDataType[] } {
    const meshes: THREE.Mesh[] = []
    this.bindingModel.traverse(child => {
      if ((child as THREE.Mesh).isMesh) {
        meshes.push(child as THREE.Mesh)
      }
    })

    return { meshes, animationsData: this._animationData }
  }

  get mixers (): THREE.AnimationMixer[] {
    return this._mixers
  }

  setModelScalar = (scalar: number) => {
    this.scale.setScalar(this.userData.defaultScalar * scalar)

    this.updateModel()
  }

  reset = () => {
    this.scale.setScalar(this.userData.defaultScalar)
    this.position.set(0, 0, 0)

    this.updateModel()
  }

  applyModelSettings = (settings: ModelSettings) => {
    const { position, scale, rotation } = settings
    if (position) {
      this.position.set(position.x, position.y, position.z)
    }
    if (scale) {
      this.scale.set(scale.x, scale.y, scale.z)
    }
    if (rotation) {
      this.rotation.set(rotation.x, rotation.y, rotation.z)
    }
  }

  static attachMapToMesh = async (mesh: THREE.Mesh, texture: THREE.Texture, attachMapEnum: AttachMapEnum) => {
    switch (attachMapEnum) {
      case AttachMapEnum.texture:
        ModelBox3.attachTextureToMesh(mesh, texture).then(() => console.log('texture added!'))
        break
      case AttachMapEnum.bumpMap:
        ModelBox3.attachBumpMapToMesh(mesh, texture).then(() => console.log('bump map added'))
        break
      case AttachMapEnum.normalMap:
        ModelBox3.attachNormalMapToMesh(mesh, texture).then(() => console.log('normal map added'))
        break
      case AttachMapEnum.specularMap:
        ModelBox3.attachSpecularMapToMesh(mesh, texture).then(() => console.log('specular map added'))
        break
      default:
    }

    if (!Array.isArray(mesh.material)) {
      mesh.material.needsUpdate = true
    }
  };

  static attachTextureToMesh = async (mesh: THREE.Mesh, texture: THREE.Texture) => {
    if (!Array.isArray(mesh.material)) {
      // @ts-ignore
      mesh.material.map = texture
    }
  }

  static attachBumpMapToMesh = async (mesh: THREE.Mesh, texture: THREE.Texture) => {
    if (!Array.isArray(mesh.material)) {
      // @ts-ignore
      mesh.material.bumpMap = texture
      // @ts-ignore
      mesh.material.bumpScale = 0.03
    }
  }

  static attachNormalMapToMesh = async (mesh: THREE.Mesh, texture: THREE.Texture) => {
    if (!Array.isArray(mesh.material)) {
      // @ts-ignore
      mesh.material.normalMap = texture
      // @ts-ignore
      mesh.material.normalScale.set(2, 2)
    }
  }

  static attachSpecularMapToMesh = async (mesh: THREE.Mesh, texture: THREE.Texture) => {
    if (!Array.isArray(mesh.material)) {
      // @ts-ignore
      mesh.material.specularMap = texture
    }
  }
}

export default ModelBox3
