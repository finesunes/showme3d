import * as THREE from 'three'
import FileLoader from '../FileLoader'

import throttle from 'lodash/throttle'

class SceneFacade extends THREE.Scene {
  private axesHelper: THREE.AxesHelper | null = null
  private gridHelper: THREE.GridHelper | null = null
  ambientLight: THREE.AmbientLight | null = null
  private plane: THREE.Mesh | null
  cameraLight: THREE.PointLight | null

  private _planeSize: number = 10
  private _planeScalar: number = 1
  private _planeMaterial: THREE.MeshPhongMaterial = new THREE.MeshPhongMaterial()
  planeTextureUrl?: string

  backgroundTextureUrl?: string | string[]
  backgroundColor?: THREE.Color

  private render: undefined | (() => void)
  private autoAnimate: boolean = true

  customFog: null | THREE.Fog = null

  constructor () {
    super()

    this.axesHelper = null
    this.gridHelper = null
    this.ambientLight = null
    this.cameraLight = null
    this.plane = null
  }

  setRenderFunc = (render: () => void) => {
    this.render = render
  }

  forceRender = throttle(() => {
    if (!this.autoAnimate && this.render) {
      this.render()
    }
  }, 100)

  setAutoAnimate = (autoAnimate: boolean) => {
    this.autoAnimate = autoAnimate
  }

  get isExistPlane (): boolean {
    return !!this.plane
  }

  enablePlane = (size: number = this._planeSize): SceneFacade => {
    this.disablePlane()

    if (this.plane) {
      return this
    }

    const planeGeometry = new THREE.PlaneBufferGeometry(size, size)

    this.plane = new THREE.Mesh(
      planeGeometry,
      this._planeMaterial
    )
    this.plane.scale.setScalar(this._planeScalar)
    this.plane.position.y = -0.01
    this.plane.rotateX(-Math.PI / 2)
    this.plane.receiveShadow = true

    this.add(this.plane)

    this.forceRender()

    return this
  }

  disablePlane = (): SceneFacade => {
    if (this.plane) {
      this.remove(this.plane)
      this.plane = null

      this.forceRender()
    }

    return this
  }

  get planeScalar (): number {
    return this._planeScalar
  }

  set planeScalar (scalar: number) {
    this._planeScalar = scalar
    if (this.plane) {
      this.plane.scale.setScalar(this._planeScalar)

      this.forceRender()
    }
  }

  get planeColor (): THREE.Color {
    return this._planeMaterial.color
  }

  set planeColor (color: THREE.Color) {
    this._planeMaterial.color = color

    this.forceRender()
  }

  get planeTexture (): THREE.Texture | null {
    return this._planeMaterial.map
  }

  set planeTexture (texture: THREE.Texture | null) {
    this._planeMaterial.map = texture
    this._planeMaterial.needsUpdate = true

    this.forceRender()
  }

  get isExistAxesHelper (): boolean {
    return !!this.axesHelper
  }

  enableAxesHelper = (size: number = 15): SceneFacade => {
    this.disableAxesHelper()

    this.axesHelper = new THREE.AxesHelper(size)
    this.add(this.axesHelper)

    this.forceRender()

    return this
  };

  disableAxesHelper = (): SceneFacade => {
    if (this.axesHelper) {
      this.remove(this.axesHelper)
      this.axesHelper = null

      this.forceRender()
    }

    return this
  };

  get isExistGridHelper (): boolean {
    return !!this.gridHelper
  }

  enableGridHelper = (
    size: number = 10,
    division: number = 10,
    color1: THREE.ColorRepresentation = 0x444444,
    color2: THREE.ColorRepresentation = 0x888888
  ): SceneFacade => {
    this.disableGridHelper()

    this.gridHelper = new THREE.GridHelper(size, division, color1, color2)
    this.add(this.gridHelper)

    this.forceRender()

    return this
  };

  disableGridHelper = (): SceneFacade => {
    if (this.gridHelper) {
      this.remove(this.gridHelper)
      this.gridHelper = null

      this.forceRender()
    }

    return this
  };

  get isExistCameraLight (): boolean {
    return !!this.cameraLight
  }

  enableCameraLight = (intensity: number = 1, color: THREE.ColorRepresentation = 0x999999): SceneFacade => {
    this.disableCameraLight()

    this.cameraLight = new THREE.PointLight(color, intensity)
    this.add(this.cameraLight)

    this.forceRender()

    return this
  };

  setCameraLightIntensity = (intensity: number = 1) => {
    if (this.cameraLight) {
      this.cameraLight.intensity = intensity

      this.forceRender()
    }
  }

  disableCameraLight = (): SceneFacade => {
    if (this.cameraLight) {
      this.remove(this.cameraLight)
      this.cameraLight = null

      this.forceRender()
    }

    return this
  };

  get isExistAmbientLight (): boolean {
    return !!this.ambientLight
  }

  enableAmbientLight = (intensity: number = 0.5): SceneFacade => {
    this.disableAmbientLight()

    this.ambientLight = new THREE.AmbientLight()
    this.ambientLight.intensity = intensity
    this.add(this.ambientLight)

    this.forceRender()

    return this
  };

  setAmbientIntensity = (intensity: number = 0.1) => {
    if (this.ambientLight) {
      this.ambientLight.intensity = intensity

      this.forceRender()
    }
  }

  disableAmbientLight = (): SceneFacade => {
    if (this.ambientLight) {
      this.remove(this.ambientLight)
      this.ambientLight = null

      this.forceRender()
    }

    return this
  };

  get isExistFog (): boolean {
    return !!this.fog
  }

  enableFog = (far: number = 40, near: number = 0.1, color: THREE.ColorRepresentation = 0xffffff): SceneFacade => {
    this.disableFog()

    const fog = new THREE.Fog(color, near, far)

    this.fog = fog
    this.customFog = fog

    this.forceRender()

    return this
  };

  setFogFar = (far: number = 40): SceneFacade => {
    if (this.customFog) {
      this.customFog.far = far
    }

    this.forceRender()

    return this
  }

  disableFog = (): SceneFacade => {
    if (this.fog) {
      this.fog = null

      this.forceRender()
    }

    return this
  };

  setPlaneBackground = async (value: File | string | THREE.Color | null) => {
    if (value instanceof File || typeof value === 'string') {
      const loader = new FileLoader()
      this.planeTexture = await loader.loadTexture(value)

      if (typeof value === 'string') {
        this.planeTextureUrl = value
      }
    }

    if (value instanceof THREE.Color) {
      this.planeColor = value
    }

    if (value === null) {
      this.planeTexture = null
      this.planeTextureUrl = undefined
    }

    this.forceRender()
  }

  setBackground = async (value: (File | string)[] | File | string | THREE.Color | null) => {
    if (Array.isArray(value) && value.length === 6) {
      const loader = new FileLoader()
      this.background = await loader.loadCubeTexture(value)

      if (value.every((v: File | string) => typeof v === 'string')) {
        this.backgroundTextureUrl = value as string[]
        this.backgroundColor = undefined
      }
    }

    if (value instanceof File || typeof value === 'string') {
      const loader = new FileLoader()
      this.background = await loader.loadTexture(value)

      if (typeof value === 'string') {
        this.backgroundTextureUrl = value
        this.backgroundColor = undefined
      }
    }

    if (value instanceof THREE.Color) {
      this.background = value
      this.backgroundColor = value
      this.backgroundTextureUrl = undefined
    }

    if (value === null) {
      this.background = new THREE.Color('#FFF')

      this.backgroundTextureUrl = undefined
      this.backgroundColor = undefined
    }

    this.forceRender()
  }
}

export default SceneFacade
