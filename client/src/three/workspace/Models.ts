import * as THREE from 'three'
import ModelBox3, { AnimationsDataType } from './ModelBox3'
import SceneFacade from './SceneFacade'
import FileLoader, { LoaderResponseType, LoadingData } from '../FileLoader'
import { AttachMapEnum, AttachMapsToMeshType } from '@utils'
import map from 'lodash/map'
import { testFileIsImg } from '@utils/file'
import { ArchiveDataType } from './WorkspaceSettings'

export type ModelDataType = {
  name: string,
  meshes: THREE.Mesh[],
  animationsData: AnimationsDataType[]
}

class Models extends Array<ModelBox3> {
  scene: SceneFacade

  private _shadows: boolean = false

  private render: undefined | (() => void)
  private autoAnimate: boolean = true

  constructor (scene: SceneFacade) {
    super()

    this.scene = scene
  }

  add (...objects: LoaderResponseType[]): void {
    if (objects.length === 1) {
      this.addOne(objects[0])
    } else {
      objects.forEach(object => this.addOne(object))
    }
  }

  addOne = (object: LoaderResponseType): void => {
    const modelBox = this.wrapInBox(object.value)

    if (object.url) {
      modelBox.userData.url = object.url
    }
    if (object.archiveUrl) {
      modelBox.userData.archiveUrl = object.archiveUrl
    }
    if (object.name) {
      modelBox.userData.name = object.name
    }

    this.normalizeSize(modelBox)
    if (object.settings) {
      modelBox.applyModelSettings(object.settings)
    }

    modelBox.updateModel()

    this.push(modelBox)

    this.scene.add(modelBox)
    this.scene.add(modelBox.bindingModel)

    if (modelBox.skeleton) {
      this.scene.add(modelBox.skeleton)
    }
  }

  normalizeSize = (object: THREE.Object3D): void => {
    const size: THREE.Vector3 = new THREE.Box3().setFromObject(object).getSize(new THREE.Vector3())
    const maxAxis: number = Math.max(size.x, size.y, size.z)

    object.userData.defaultScalar = 4 / maxAxis

    object.scale.setScalar(object.userData.defaultScalar)
  }

  wrapInBox = (object: THREE.Object3D): ModelBox3 => {
    return new ModelBox3(object).init()
  }

  findByUuid = (uuid: string): ModelBox3 | undefined => {
    return this.find(modelBox3 => modelBox3.uuid === uuid)
  }

  setScaleModel = (uuid: string, scalar: number) => {
    this.findByUuid(uuid)?.setModelScalar(scalar)

    this.forceRender()
  }

  resetModelByUuid = (uuid: string) => {
    this.findByUuid(uuid)?.reset()

    this.forceRender()
  }

  getAllModelsData = () => {
    const data: Record<string, ModelDataType> = {}

    this.forEach(modelBox => {
      const modelBox3Data = modelBox.allChildrenData

      data[modelBox.uuid] = {
        name: modelBox.fileName || '',
        meshes: modelBox3Data.meshes,
        animationsData: modelBox3Data.animationsData
      }
    })

    return data
  }

  get allMixers (): THREE.AnimationMixer[] {
    return this.reduce((memo, model) => memo.concat(model.mixers), [] as THREE.AnimationMixer[])
  }

  get shadows (): boolean {
    return this._shadows
  }

  set shadows (value: boolean) {
    this._shadows = value

    this.forEach(modelBox => {
      modelBox.bindingModel.traverse(child => {
        if ((child as THREE.Mesh).isMesh) {
          child.castShadow = value
          child.receiveShadow = value
        }
      })
    })
  }

  setVisibleSkeletonHelper = async (value: boolean) => {
    this.forEach(modelBox3 => {
      modelBox3.skeleton.visible = value
    })
  }

  removeModelByUuid = (uuid: string) => {
    const modelIndex = this.findIndex(modelBox3 => modelBox3.uuid === uuid)
    const model = this[modelIndex]

    if (model) {
      this.scene.remove(model.bindingModel, model)
      this.splice(modelIndex, 1)
    }
  }

  setRenderFunc = (render: () => void) => {
    this.render = render
  }

  forceRender = () => {
    if (!this.autoAnimate && this.render) {
      this.render()
    }
  }

  setAutoAnimate = (autoAnimate: boolean) => {
    this.autoAnimate = autoAnimate
  }

  attachMapToMesh = async (uuid: string, attachedFiles: AttachMapsToMeshType) => {
    const mesh = this.scene.getObjectByProperty('uuid', uuid) as THREE.Mesh

    if (mesh.isMesh) {
      const loader = new FileLoader()

      map(attachedFiles, async (file, key) => {
        if (file && testFileIsImg(file)) {
          const texture = await loader.loadTexture(file)
          await ModelBox3.attachMapToMesh(mesh, texture, key as AttachMapEnum)
        }
      })
    }
  }

  getModelsShareData = () => {
    const models: LoadingData[] = []
    const archives: ArchiveDataType[] = []

    this.forEach(modelBox3 => {
      const position = modelBox3.position
      const scale = modelBox3.scale
      const rotation = modelBox3.rotation

      const modelSettings = {
        animation: '',
        position: {
          x: position.x,
          y: position.y,
          z: position.z
        },
        scale: {
          x: scale.x,
          y: scale.y,
          z: scale.z
        },
        rotation: {
          x: rotation.x,
          y: rotation.y,
          z: rotation.z
        }
      }

      if (modelBox3.userData.archiveUrl) {
        const archiveUrl = modelBox3.userData.archiveUrl

        const archiveIndex = archives.findIndex(archive => archive.url === archiveUrl)
        if (archiveIndex > 0) {
          archives[archiveIndex].models[modelBox3.userData.name] = modelSettings
        } else {
          archives.push({
            url: archiveUrl,
            models: {
              [modelBox3.userData.name]: modelSettings
            }
          })
        }
      } else {
        models.push({
          src: modelBox3.userData.url,
          settings: modelSettings
        } as LoadingData)
      }
    })

    return { models, archives }
  }
}

export default Models
