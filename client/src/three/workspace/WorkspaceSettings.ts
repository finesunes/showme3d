import * as THREE from 'three'
import { Color, Fog } from 'three'
import URI from 'urijs'
import Workspace from './index'
import { LoadingData } from '../FileLoader'
import { ModelSettings } from '@utils'
import { toast } from 'react-toastify'
import { loadAndUnarchive } from '@utils/url'

export type ArchiveDataType = {
  url: string,
  models: {
    [key: string]: ModelSettings
  }
}

export type SceneSettingsType = {
  autoAnimate?: '0' | '1'
  grid?: '0' | '1'
  axes?: '0' | '1'
  fog?: '0' | '1'
  fogFar?: number
  cl?: '0' | '1' // camera light
  clI?: number // camera light intensity
  al?: '0' | '1' // ambient light
  alI?: number // ambient light intensity

  plane?: '0' | '1'
  planeSize?: number
  planeBC?: string | THREE.ColorRepresentation // plane background color
  planeBI?: string // plane background image

  BC?: string | THREE.ColorRepresentation // background color
  BI?: string | string[] // background image

  models?: string
  archives?: string
}

const getSettingsFromUrl = () => {
  return URI.parseQuery(new URI(window.location.href).search()) as SceneSettingsType
}

const applyUrlSettingsToWorkspace = async (workspace: Workspace) => {
  if (!workspace) return

  const settings = getSettingsFromUrl()

  // if (Object.getOwnPropertyNames(settings).length === 0) return

  workspace.autoAnimate = settings.autoAnimate === '1'

  if (settings.grid === '1') {
    workspace.scene.enableGridHelper()
  }
  if (settings.axes === '1') {
    workspace.scene.enableAxesHelper()
  }
  if (settings.fog === '1') {
    if (Number(settings.fogFar) >= 0) {
      workspace.scene.enableFog(Number(settings.fogFar))
    } else {
      workspace.scene.enableFog()
    }
  }

  // camera light
  if (settings.cl === '1') {
    if (Number(settings.clI) >= 0) {
      workspace.scene.enableCameraLight(Number(settings.clI))
    } else {
      workspace.scene.enableCameraLight()
    }
  } else if (typeof settings.cl === 'undefined') {
    workspace.scene.enableCameraLight()
  }

  // ambient light
  if (settings.al === '1') {
    if (Number(settings.alI) >= 0) {
      workspace.scene.enableAmbientLight(Number(settings.alI))
    } else {
      workspace.scene.enableAmbientLight()
    }
  } else if (typeof settings.alI === 'undefined') {
    workspace.scene.enableAmbientLight()
  }

  // plane
  if (settings.plane === '1') {
    workspace.scene.enablePlane()
  }
  if (settings.planeSize && Number(settings.planeSize) > 0) {
    workspace.scene.planeScalar = Number(settings.planeSize)
  }
  if (settings.planeBC) {
    workspace.scene.setPlaneBackground(new Color(`#${settings.planeBC}`)).then(() => {})
  }
  if (settings.planeBI) {
    workspace.scene.setPlaneBackground(settings.planeBI).then(() => workspace.render())
  }

  if (settings.BC) {
    workspace.scene.setBackground(new Color(`#${settings.BC}`)).then(() => {})
  }
  if (settings.BI) {
    workspace.scene.setBackground(settings.BI).then(() => {})
  }

  if (settings.models) {
    try {
      const loadingData: LoadingData[] = JSON.parse(settings.models)

      await workspace.loadFiles(loadingData)
    } catch (e) {
      toast.error(e)
      console.log(e)
    }
  }

  if (settings.archives) {
    try {
      const archivesData: ArchiveDataType[] = JSON.parse(settings.archives)
      const archivesUrls = archivesData.map(archiveData => archiveData.url)

      const archiveModelFiles = await loadAndUnarchive(archivesUrls)

      const loadingData: LoadingData[] = archiveModelFiles.map(modelFile => {
        const { archiveUrl, settings: modelSettings } = findFileDataInArchivesData(archivesData, modelFile)

        return {
          src: modelFile,
          archiveUrl,
          settings: modelSettings
        }
      })

      await workspace.loadFiles(loadingData)
    } catch (e) {
      toast.error(e)
      console.log(e)
    }
  }

  workspace.render()
}

const findFileDataInArchivesData = (archivesData: ArchiveDataType[], modelFile: File): {
  archiveUrl: string
  settings: ModelSettings
} => {
  for (const archiveDataKey in archivesData) {
    const archiveData = archivesData[archiveDataKey]
    for (const modelName in archiveData.models) {
      if (modelName === modelFile.name) {
        return {
          archiveUrl: archiveData.url,
          settings: archiveData.models[modelName]
        }
      }
    }
  }

  return { archiveUrl: '', settings: {} }
}

const getWorkspaceSettings = (workspace: Workspace): SceneSettingsType => {
  if (!workspace) return {}

  const { models, archives } = workspace.models.getModelsShareData()

  const data: SceneSettingsType = {
    autoAnimate: workspace.autoAnimate ? '1' : '0',
    grid: workspace.scene.isExistGridHelper ? '1' : '0',
    axes: workspace.scene.isExistAxesHelper ? '1' : '0',
    fog: workspace.scene.isExistFog ? '1' : '0',
    fogFar: (workspace.scene.fog as Fog)?.far || 0,
    cl: workspace.scene.isExistCameraLight ? '1' : '0',
    clI: workspace.scene.cameraLight?.intensity || 0,
    al: workspace.scene.isExistAmbientLight ? '1' : '0',
    alI: workspace.scene.ambientLight?.intensity || 0,

    plane: workspace.scene.isExistPlane ? '1' : '0',
    planeSize: workspace.scene.isExistPlane ? workspace.scene.planeScalar : undefined,
    planeBC: workspace.scene.isExistPlane ? workspace.scene.planeColor.getHexString() : undefined,
    planeBI: workspace.scene.planeTextureUrl,

    BC: workspace.scene.backgroundColor?.getHexString(),
    BI: workspace.scene.backgroundTextureUrl,

    models: JSON.stringify(models),
    archives: JSON.stringify(archives)
  }

  Object.keys(data).forEach((key) => {
    const _key = key as keyof SceneSettingsType
    if (!data[_key] || data[_key] === '0') {
      delete data[_key]
    }
  })

  return data
}

const createShareUrl = (workspace: Workspace): string => {
  return new URI(window.location.href).search(getWorkspaceSettings(workspace)).toString()
}

export {
  getSettingsFromUrl,
  applyUrlSettingsToWorkspace,
  getWorkspaceSettings,
  createShareUrl
}
