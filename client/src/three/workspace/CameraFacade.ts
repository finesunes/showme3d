import * as THREE from 'three'

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { DragControls } from 'three/examples/jsm/controls/DragControls'
import { TransformControls } from 'three/examples/jsm/controls/TransformControls'
import ModelBox3 from './ModelBox3'
import SceneFacade from './SceneFacade'

enum DragEventType {
  hoverOn = 'hoveron',
  hoverOff = 'hoveroff',
  dragStart = 'dragstart',
  dragEnd = 'dragend'
}

export enum ControlType {
  drag = 'drag',
  translate = 'translate',
  rotate = 'rotate',
  scale = 'scale',
}

class CameraFacade extends THREE.PerspectiveCamera {
  scene: SceneFacade
  domElement: HTMLElement

  orbitControls: OrbitControls | null = null

  dragControls: DragControls | null = null

  transformControls: TransformControls | null = null
  selectedObject: THREE.Object3D | null = null
  pickRaycaster: THREE.Raycaster = new THREE.Raycaster()
  pickableObjects: any[] = []
  intersections: THREE.Intersection[] = []

  activeControl: string | null = null

  private autoAnimate: boolean = true
  private render: undefined | (() => void) = undefined

  constructor (
    scene: SceneFacade, domElement: HTMLElement,
    fov: number = 75, aspect: number = 1, near: number = 0.1, far: number = 200
  ) {
    super(fov, aspect, near, far)

    this.scene = scene
    this.domElement = domElement
  }

  setRenderFunc = (render: () => void) => {
    this.render = render
  }

  forceRender = () => {
    if (!this.autoAnimate && this.render) {
      this.render()
    }
  }

  setAutoAnimate = (autoAnimate: boolean) => {
    this.autoAnimate = autoAnimate

    if (!autoAnimate) {
      this.orbitControls?.addEventListener('change', this.changeOrbitControls)
      this.dragControls?.addEventListener('drag', this.forceRender)

      this.domElement.addEventListener('mousemove', this.mouseMoveHandleAnimate)
    } else {
      this.orbitControls?.removeEventListener('change', this.changeOrbitControls)
      this.dragControls?.removeEventListener('drag', this.forceRender)

      this.domElement.removeEventListener('mousemove', this.mouseMoveHandleAnimate)
    }
  }

  changeOrbitControls = () => {
    if (this.scene && this.scene.cameraLight) {
      this.scene.cameraLight.position.copy(this.position)
    }

    this.forceRender()
  }

  mouseMoveHandleAnimate = () => {
    if (this.transformControls?.object instanceof ModelBox3 && !this.orbitControls?.enabled) {
      this.forceRender()
    }
  }

  createOrbitControls = (): CameraFacade => {
    this.orbitControls = new OrbitControls(this, this.domElement)

    return this
  };

  // DRAG
  createDragControls = (objects: THREE.Object3D[]): CameraFacade => {
    this.dragControls = new DragControls(objects, this, this.domElement)
    this.dragControls.enabled = false
    return this
  };

  enableDragControls = (): CameraFacade => {
    this.disableAllControls()

    if (!this.dragControls) {
      return this
    }

    this.addDragEventsListeners()

    if (!this.dragControls.enabled) {
      this.dragControls.enabled = true
    }

    this.activeControl = ControlType.drag

    return this
  };

  disableDragControls = (): CameraFacade => {
    if (!this.dragControls) {
      return this
    }

    this.dragControls.enabled = false
    this.removeDragEventsListeners()

    return this
  };

  addDragEventsListeners = (): CameraFacade => {
    if (!this.dragControls) {
      return this
    }

    this.removeDragEventsListeners()

    this.dragControls.addEventListener(DragEventType.hoverOn, this.dragListenerHoverOn)
    this.dragControls.addEventListener(DragEventType.hoverOff, this.dragListenerHoverOff)
    this.dragControls.addEventListener(DragEventType.dragStart, this.dragListenerDragStart)
    this.dragControls.addEventListener(DragEventType.dragEnd, this.dragListenerDragEnd)

    return this
  };

  removeDragEventsListeners = (): CameraFacade => {
    if (!this.dragControls) {
      return this
    }

    this.dragControls.removeEventListener(DragEventType.hoverOn, this.dragListenerHoverOn)
    this.dragControls.removeEventListener(DragEventType.hoverOff, this.dragListenerHoverOff)
    this.dragControls.removeEventListener(DragEventType.dragStart, this.dragListenerDragStart)
    this.dragControls.removeEventListener(DragEventType.dragEnd, this.dragListenerDragEnd)

    return this
  };

  dragListenerHoverOn = (e: THREE.Event) => {
    if (e.object instanceof ModelBox3) {
      e.object.visibleBox = true
    }

    this.forceRender()
  }

  dragListenerHoverOff = (e: THREE.Event) => {
    if (this.orbitControls) {
      this.orbitControls.enabled = true
    }

    if (e.object instanceof ModelBox3) {
      e.object.visibleBox = false
    }

    this.forceRender()
  }

  dragListenerDragStart = (e: THREE.Event) => {
    if (this.orbitControls) {
      this.orbitControls.enabled = false
    }

    if (e.object instanceof ModelBox3 && !e.object.visibleBox) {
      e.object.visibleBox = true
    }

    this.forceRender()
  }

  dragListenerDragEnd = (e: THREE.Event) => {
    if (e.object instanceof ModelBox3) {
      e.object.updateModelPosition()
      e.object.visibleBox = false
    }

    if (this.orbitControls) {
      this.orbitControls.enabled = true
    }

    this.forceRender()
  }

  // TRANSFORM
  createTransformControls = (objects: THREE.Object3D[]): CameraFacade => {
    this.transformControls = new TransformControls(this, this.domElement)
    this.transformControls.mode = 'translate'
    this.pickableObjects = objects
    this.transformControls.enabled = false
    return this
  };

  enableTransformControls = (mode: string = ControlType.translate): CameraFacade => {
    this.disableAllControls()

    if (!this.transformControls) {
      return this
    }

    this.addTransformEventsListeners()

    if (!this.transformControls.enabled) {
      this.transformControls.enabled = true
    }

    this.transformControls.mode = mode

    this.activeControl = mode

    return this
  };

  disableTransformControls = (): CameraFacade => {
    if (!this.transformControls) {
      return this
    }

    this.transformControls.enabled = false
    this.removeTransformEventsListeners()

    return this
  };

  addTransformEventsListeners = (): CameraFacade => {
    if (!this.transformControls) {
      return this
    }

    this.removeTransformEventsListeners()

    this.domElement.addEventListener('pointerdown', this.onElementMouseDown, false)
    this.transformControls.addEventListener('dragging-changed', this.transformListenerDraggingChanged)

    return this
  };

  removeTransformEventsListeners = (): CameraFacade => {
    if (!this.transformControls) {
      return this
    }

    if (this.selectedObject instanceof ModelBox3) {
      this.selectedObject.visibleBox = false
      this.selectedObject = null
    }

    this.scene.remove(this.transformControls)

    this.domElement.removeEventListener('pointerdown', this.onElementMouseDown, false)
    this.transformControls.removeEventListener('dragging-changed', this.transformListenerDraggingChanged)

    return this
  };

  transformListenerDraggingChanged = () => {
    if (this.orbitControls) {
      this.orbitControls.enabled = !this.orbitControls.enabled

      if (this.orbitControls.enabled && this.transformControls?.object instanceof ModelBox3) {
        this.transformControls.object.updateModel()
      }

      this.forceRender()
    }
  }

  onElementMouseDown = (event: MouseEvent) => {
    this.pickRaycaster.setFromCamera(
      {
        x: (event.clientX / this.domElement.clientWidth) * 2 - 1,
        y: -(event.clientY / this.domElement.clientHeight) * 2 + 1
      }, this)
    this.intersections = this.pickRaycaster.intersectObjects(this.pickableObjects, false)

    if (this.intersections.length > 0) {
      if (this.selectedObject && this.selectedObject instanceof ModelBox3) {
        this.selectedObject.visibleBox = false
      }

      this.selectedObject = this.intersections[0].object

      this.lookAt(this.selectedObject.position)

      if (this.transformControls) {
        this.transformControls.attach(this.selectedObject)
        this.scene.add(this.transformControls)
      }

      if (this.selectedObject instanceof ModelBox3) {
        this.selectedObject.visibleBox = true
      }
    }

    this.forceRender()
  }

  // ALL
  disableAllControls = () => {
    this.disableDragControls().disableTransformControls()

    this.activeControl = null

    this.forceRender()
  }
}

export default CameraFacade
